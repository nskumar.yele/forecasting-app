# api


## Backend 

* In docker-stack.yml change the context to your working directory of the project and values of the environment variables 

* Start the stack with Docker Compose:

```bash
docker-compose -f docker-stack.yml up --build --force-recreate
```
