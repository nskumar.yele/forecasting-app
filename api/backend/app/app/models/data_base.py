from sqlalchemy import (Column, String, Integer, Identity, Enum, DateTime, Text, ForeignKey)
from sqlalchemy.orm import relationship

from app.db.base_class import Base
from app.schemas.data_source import DataSourceTypeEnum


class UniqueIdentities:
  id = Column(Integer, Identity(start=1, cycle=True), primary_key=True)
  ref_id = Column(String, unique=True)


class CustomBaseModel:
  created_at = Column(DateTime)
  updated_at = Column(DateTime)
  created_by = Column(String)
  updated_by = Column(String)


class DataSources(Base, UniqueIdentities, CustomBaseModel):
  __tablename__ = "DATA_SOURCE"

  name = Column(String)
  desc = Column(Text)
  type = Column(Enum(DataSourceTypeEnum, values_callable=lambda x: [e.value for e in x]))
  params = relationship("SourceParameters",
                        backref="params",
                        cascade="all, delete-orphan",
                        lazy='joined')


class SourceParameters(Base, UniqueIdentities):
  __tablename__ = "DATA_SOURCE_PARAMETERS"

  name = Column(String)
  value = Column(String)
  data_source_id = Column(Integer, ForeignKey(DataSources.id, ondelete='CASCADE'))


class Model(Base, CustomBaseModel):
  __tablename__ = "MODEL"

  id = Column(String, primary_key=True, unique=True)
  ref_id = Column(String, unique=True)
  data_source_id = Column(Integer, ForeignKey(DataSources.id, ondelete='CASCADE'))
  name = Column(String, unique=True)
  type = Column(String)
  desc = Column(Text)
  versions = relationship("ModelVersion",
                          backref="versions",
                          cascade="all, delete-orphan",
                          lazy='joined')


class ModelVersion(Base):
  __tablename__ = "MODEL_VERSION"

  id = Column(String, unique=True, primary_key=True)
  ref_id = Column(String, unique=True)
  model_id = Column(String, ForeignKey(Model.id, ondelete='CASCADE'))
  deployment_name = Column(String)
  workflow_name = Column(String)
  version = Column(Integer)
  created_by = Column(String)
  created_at = Column(DateTime)
  status = Column(String)
  status_message = Column(String)
  updated_at = Column(DateTime)
  updated_by = Column(String)
  performance_metrics = Column(String)
  feature_significance = Column(String)
  parameters = relationship("ModelParameters",
                            backref="parameters",
                            cascade="all, delete-orphan",
                            lazy='joined')


class ModelParameters(Base, UniqueIdentities):
  __tablename__ = "MODEL_PARAMETERS"

  model_version_id = Column(String, ForeignKey(ModelVersion.id, ondelete='CASCADE'))
  parameter = Column(String)
  value = Column(String)


class ForecastingResult(Base):
  __tablename__ = "FORECASTING_RESULT"

  id = Column(Integer, Identity(start=1, cycle=True), primary_key=True)
  actual = Column(Integer)
  forecast = Column(Integer)
  sku = Column(String)
  model_version_id = Column(String, ForeignKey(ModelVersion.id, ondelete='CASCADE'))
  date = Column(DateTime)


class Notification(Base, CustomBaseModel):
  __tablename__ = "NOTIFICATION"

  id = Column(Integer, Identity(start=1, cycle=True), primary_key=True)
  ref_id = Column(String, unique=True)
  user = Column(String)
  status = Column(String)
  message = Column(String)
  severity = Column(String)
  source = Column(String)
