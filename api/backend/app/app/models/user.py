from sqlalchemy import Boolean, Column, String
from sqlalchemy.orm import relationship
from app.db.base_class import Base


class User(Base):
  id = Column(String, primary_key=True, index=True)
  full_name = Column(String, index=True)
  email = Column(String, unique=True, index=True)
  hashed_password = Column(String)
  is_active = Column(Boolean(), default=True)
  is_superuser = Column(Boolean(), default=False)
  items = relationship("Item", back_populates="owner")
