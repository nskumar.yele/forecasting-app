from datetime import timedelta
from functools import wraps
import time
import jwt
from fastapi import Request, Response, status
from jwt import InvalidTokenError
from app.logger import logger


def token_required(func):

  @wraps(func)
  def middle_ware(request: Request, response: Response, *args, **kwargs):
    try:
      token = request.headers['authorization'].split(' ')[1]
      payload = jwt.decode(token, verify=False)
      logger.debug("Payload retrieved from decoding the token {}".format(payload))
      expiry = payload['exp']
      if time.time() > expiry:
        logger.debug('Token expiration time: {}'.format(str(timedelta(seconds=time.time() -
                                                                      expiry))))
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return {'title': 'Unauthorized', 'status': 401, 'detail': 'Token has expired'}
      CurrentUser(payload['email'], payload['cognito:username'])
    except InvalidTokenError as e:
      logger.error('Occur invalid token error: ', e)
      response.status_code = status.HTTP_401_UNAUTHORIZED
      return {'title': 'Unauthorized', 'status': 401, 'detail': 'Invalid token'}
    except Exception as e:
      logger.error('Occur token missing error: ', e)
      response.status_code = status.HTTP_401_UNAUTHORIZED
      return {'title': 'Unauthorized', 'status': 401, 'detail': 'Token is missing!'}
    if request.method == "GET" or request.method == "DELETE":
      return func(request, response, *args, **kwargs)
    return func(request, response, *args, **kwargs)

  return middle_ware


class CurrentUser:
  __email = ""
  __username = ""

  def __init__(self, email: str, username: str):
    logger.info("current User details {}, {}".format(email, username))
    CurrentUser.__email = email
    CurrentUser.__username = username

  @staticmethod
  def get_current_user_email():
    return CurrentUser.__email
