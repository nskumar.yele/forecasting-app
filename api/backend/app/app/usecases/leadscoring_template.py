from app.logger import logger
from app.schemas.models import WorkflowData
from app.usecases.workflow_template import WorkflowTemplate


class LeadScoringTemplate(WorkflowTemplate):

  def generate(self, data: WorkflowData):
    logger.info("recieved data {}".format(data))
    template = {
        "name": "train-deploy-sf-lead-scoring-model",
        "number_of_runs": 0,
        "run": False,
        "schedule": {},
        "spec": {
            "project_id": "a-1504733944",
            "project_name": "lead-scoring-solution",
            "model_name": "sf-lead-scoring-model",
            "train": {
                "name":
                "train-deploy",
                "type":
                "TRAIN",
                "replicas":
                0,
                "source": {
                    "type": "GIT",
                    "git": {
                        "repository":
                        "https://gitlab.com/predera/clients/walmart/forecasting-solution.git",
                        "revision": "feature/MERGE_FEEDBACK_NOTIFICATIONS",
                        "directory": "api/backend",
                        "secret": {
                            "name": "lead-scoring-solution-git-secrets"
                        }
                    },
                    "experiment": None,
                    "model_artifact": None,
                    "registered_model": None,
                    "command": "pip install -r requirements.txt && "
                    "python ./app/app/run/usecases/lead_scoring/main.py",
                    "image": "python:3.8.13",
                    "runtime": None,
                    "model_artifact_path": None,
                    "spark_runtime": None,
                    "spark_version": None
                },
                "output":
                None,
                "docker":
                None,
                "resources":
                None,
                "env": [{
                    "name": "MODEL_VERSION_ID",
                    "value": data["model_version_id"]
                }, {
                    "name": "MODEL_VERSION_REF_ID",
                    "value": data["model_version_ref_id"]
                }, {
                    "name": "SOURCE_REF_ID",
                    "value": data["source_ref_id"]
                }, {
                    "name": "PYTHONPATH",
                    "value": "./app"
                }, {
                    "name": "AIQ_URL",
                    "value": "https://sandbox.predera.com"
                }, {
                    "name": "AIQ_USERNAME",
                    "value": "vijaykumar.guntreddy@predera.com"
                }, {
                    "name": "AIQ_PASSWORD",
                    "value": "Vijay@2057"
                }, {
                    'name': 'PROJECT_NAME',
                    'value': 'forecasting_app'
                }, {
                    'name': 'SECRET_KEY',
                    'value': 'helloworld'
                }, {
                    'name': 'FIRST_SUPERUSER',
                    'value': 'bala.sivakumari@predera.com'
                }, {
                    'name': 'FIRST_SUPERUSER_PASSWORD',
                    'value': 'helloworld'
                }, {
                    'name': 'SMTP_TLS',
                    'value': 'True'
                }, {
                    'name': 'SMTP_PORT',
                    'value': '587'
                }, {
                    'name': 'SMTP_HOST',
                    'value': 'smtp.office365.com'
                }, {
                    'name': 'SMTP_USER',
                    'value': 'bala.sivakumari@predera.com'
                }, {
                    'name': 'SMTP_PASSWORD',
                    'value': ''
                }, {
                    'name': 'EMAILS_FROM_EMAIL',
                    'value': 'bala.sivakumari@predera.com'
                }, {
                    'name': 'USERS_OPEN_REGISTRATION',
                    'value': 'True'
                }, {
                    'name': 'PGADMIN_LISTEN_PORT',
                    'value': '5050'
                }, {
                    'name': 'PGADMIN_DEFAULT_EMAIL',
                    'value': 'bala.sivakumari@predera.com'
                }, {
                    'name': 'PGADMIN_DEFAULT_PASSWORD',
                    'value': 'helloworld'
                }, {
                    'name': 'POSTGRES_SERVER',
                    'value': 'localhost'
                }, {
                    'name': 'POSTGRES_USER',
                    'value': 'postgres'
                }, {
                    'name': 'POSTGRES_PASSWORD',
                    'value': 'helloworld'
                }, {
                    'name': 'POSTGRES_DB',
                    'value': 'app'
                }, {
                    'name': 'SERVER_NAME',
                    'value': 'backend'
                }, {
                    'name': 'SERVER_HOST',
                    'value': 'http://backend'
                }, {
                    "name": "SENTRY_DSN",
                    "value": ''
                }],
                "outputs":
                None,
                "sub_type":
                "TRAIN_SIMPLE",
                "deploy_condition":
                None,
                "env_source": [{
                    "name": "lead-scoring-solution-aiq-secrets",
                    "type": "secretRef"
                }],
                "data_source":
                None
            },
        },
        "metadata": {
            "generateName": "train-deploy-sf-lead-scoring-model-",
            "labels": {}
        }
    }
    return template
