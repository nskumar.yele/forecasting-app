from app.schemas.models import LeadModelEnum, ForecastModelEnum
from app.usecases import leadscoring_template, demand_forecasting_template


class TemplateFactory:

  def create(self, type: str):
    try:
      if type in [item.value for item in list(LeadModelEnum)]:
        return leadscoring_template.LeadScoringTemplate()
      elif type in [item.value for item in list(ForecastModelEnum)]:
        return demand_forecasting_template.DemandForecastingTemplate()
    except Exception:
      raise ValueError("Unknown type {}".format(type))
