from abc import abstractmethod, ABCMeta


class WorkflowTemplate(metaclass=ABCMeta):

  @abstractmethod
  def generate(self, data: any):
    pass
