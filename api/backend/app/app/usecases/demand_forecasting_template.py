from app.schemas.models import WorkflowData
from app.usecases.workflow_template import WorkflowTemplate


class DemandForecastingTemplate(WorkflowTemplate):

  def generate(self, data: WorkflowData):
    template = {
        "name": "bigcom-demand-train-forecast-prophet",
        "number_of_runs": 0,
        "run": False,
        "schedule": {},
        "spec": {
            "project_id": "a-1504733944",
            "project_name": "lead-scoring-solution",
            "model_name": "bg-forecasting-model",
            "train": {
                "name":
                "train-deploy",
                "type":
                "TRAIN",
                "replicas":
                0,
                "source": {
                    "type":
                    "GIT",
                    "git": {
                        "repository":
                        "https://gitlab.com/predera/clients/walmart/forecasting-solution.git",
                        "revision": "feature/MERGE_FEEDBACK_NOTIFICATIONS",
                        "directory": "api/backend",
                        "secret": {
                            "name": "lead-scoring-solution-git-secrets"
                        }
                    },
                    "experiment":
                    None,
                    "model_artifact":
                    None,
                    "registered_model":
                    None,
                    "command":
                    "pip install -r ./app/app/run/usecases/demand_forecasting/requirements.txt && "
                    "python3 ./app/app/run/usecases/demand_forecasting/main.py",
                    "image":
                    "python:3.8.13",
                    "runtime":
                    None,
                    "model_artifact_path":
                    None,
                    "spark_runtime":
                    None,
                    "spark_version":
                    None
                },
                "output":
                None,
                "docker":
                None,
                "resources":
                None,
                "env": [{
                    "name": "SOURCE_REF_ID",
                    "value": data["source_ref_id"]
                }, {
                    "name": "MODEL_VERSION_REF_ID",
                    "value": data["model_version_ref_id"]
                }, {
                    "name": "PYTHONPATH",
                    "value": "./app"
                }],
                "outputs":
                None,
                "sub_type":
                "TRAIN_SIMPLE",
                "deploy_condition":
                None,
                "env_source": [{
                    "name": "lead-scoring-solution-aiq-secrets",
                    "type": "secretRef"
                }],
                "data_source":
                None
            },
        },
        "metadata": {
            "generateName": "bigcom-demand-train-forecast-prophet-",
            "labels": {}
        }
    }
    return template
