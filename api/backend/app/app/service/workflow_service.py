import json
import sys

import requests

from app.common.aiq_service import AiqService
from app.logger import logger


class WorkflowService:

  def submit(self, workflow: any):
    try:
      self.aiq_service = AiqService()
      auth_token = self.aiq_service.get_aiq_auth_token()
    except Exception as e:
      logger.error("Error occurred while getting auth_token : {}".format(e))
      sys.exit(1)

    train_url = self.aiq_service.AIQ_URL + "/aiq/api/mlworkflow-runs"
    header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + auth_token}
    try:
      logger.debug("Workflow run input data : {}".format(workflow))
      response = requests.post(url=train_url,
                               headers=header,
                               data=json.dumps(workflow),
                               verify=False)
      response.raise_for_status()
    except Exception as e:
      logger.error("Exception raised while running the workflow, Error : {}".format(e))
      sys.exit(1)
    return response.json()
