import json
from datetime import datetime

from sqlalchemy import update

from app.core.auth import CurrentUser
from app.crud.model_version_repository import ModelVersionRepositry
from app.errors.model_version_errors import ModelVersionNotFoundException, ModelVersionException
from app.logger import logger
from app.models.data_base import ModelVersion
from app.run.usecases.lead_scoring import deployment
from app.schemas.models import ModelVersionsOutput, ForecastModelEnum
from app.service.notification_service import NotificationService


class ModelVersionService:

  def __init__(self):
    self.repository = ModelVersionRepositry()
    self.notification_service = NotificationService()

  def get_model_version_by_ref_id(self, ref_id: str) -> ModelVersionsOutput:
    try:
      model_version = self.repository.get_model_version_by_ref_id(ref_id)
    except Exception as e:
      logger.error("No model version found with ref_id {}, {}".format(ref_id, e))
      raise ModelVersionNotFoundException("No model version found with ref_id {}, {}".format(
          ref_id, e))
    return ModelVersionsOutput.from_orm(model_version)

  def update_model_version(self, type: str, ref_id, workflow_name):
    model_version = self.get_model_version_by_ref_id(ref_id)
    try:
      deployment_name = ""
      if type not in [item.value for item in list(ForecastModelEnum)]:
        deployment_name = deployment.data["spec"]["project_name"] + "-" + model_version.id

      statement = update(ModelVersion).values(
          deployment_name=deployment_name,
          workflow_name=workflow_name).where(ModelVersion.ref_id == ref_id)
      self.repository.update_model_version(statement)
    except Exception as e:
      logger.error(
          "Unable to update deployment and workflow name to the model version with  ref_id  {} , {}"
          .format(ref_id, e))
      raise ModelVersionException(
          "Unable to update deployment and workflow name to the model version with  ref_id  {} , {}"
          .format(ref_id, e))

  def update_model_version_with_data(self, ref_id, data, background_tasks):
    model_version = self.get_model_version_by_ref_id(ref_id)
    created_at = model_version.created_at
    username = model_version.created_by
    input_col = model_version.parameters.input_columns

    try:
      if data.get('metrics', None):
        statement = update(ModelVersion).values(
            performance_metrics=json.dumps(data.get("metrics", None)),
            feature_significance=json.dumps(data.get("feature_significance",
                                                     None))).where(ModelVersion.ref_id == ref_id)
      elif data.get("status", None):
        for obj in input_col:
          if obj.get("sku", None):
            sku = obj.get("sku")
        status = data.get("status", None)
        if status == "RUNNING":
          parameters = {
              "user": username,
              "message": "FORECAST IS RUNNING FOR SKU(S) : {}".format(sku),
              "severity": "INFO"
          }
          self.notification_service.create_notification(parameters=parameters,
                                                        destination="PUSH_NOTIFICATION")
        elif status != "CREATED" and status != "RUNNING":
          logger.info("Calling Notification Service")
          updated_at = datetime.now()
          duration = updated_at - created_at
          parameters = {
              "status": status,
              "sku": sku,
              "duration": duration,
              "cc": None,
              "user": username
          }
          background_tasks.add_task(self.notification_service.create_notification,
                                    parameters=parameters,
                                    destination="EMAIL")
          parameters = {
              "user": username,
              "severity": "INFO",
              "message": "FORECAST HAS COMPLETED FOR SKU(S) {}".format(sku)
          }
          if status == "ERROR":
            parameters["message"] = "FORECAST HAS FAILED FOR SKU(S) {}".format(sku)
          self.notification_service.create_notification(parameters=parameters,
                                                        destination="PUSH_NOTIFICATION")
        if status and status != model_version.status:
          statement = update(ModelVersion).values(
              status=data.get("status", None),
              status_message=data.get("status_message", None),
              updated_at=datetime.now(),
              updated_by=CurrentUser.get_current_user_email()).where(ModelVersion.ref_id == ref_id)

      self.repository.update_model_version(statement)
    except Exception as e:
      logger.error("Unable to update model version with ref_id : {} , {}".format(ref_id, e))
      raise ModelVersionException("Unable to update the model version with ref_id : {} , {}".format(
          ref_id, e))

  def delete_model_version_by_id(self, ref_id: str):
    self.get_model_version_by_ref_id(ref_id)
    try:
      self.repository.delete_model_version_by_id(ref_id)
    except Exception as e:
      logger.error("Unable to delete Model version  with ref_id {}, {} ".format(ref_id, e))
      raise ModelVersionException("Unable to delete Model version with ref_id {}, {}".format(
          ref_id, e))
