import uuid
from datetime import datetime
from typing import List

from sqlalchemy import update

from app.core.auth import CurrentUser
from app.crud.datasource_repository import DataSourceRepository
from app.errors.datasource_errors import DataSourceException, DataSourceNotFoundException
from app.logger import logger
from app.models.data_base import DataSources, SourceParameters
from app.schemas.data_source import Source, DataSourceTypeEnum
from app.run.usecases.lead_scoring.data_prep.sales_force_data_source import SalesforceDataSource
from app.run.usecases.lead_scoring.data_prep.snow_flake_data_source import SnowflakeDataSource


class DataSourceService:

  def __init__(self):
    self.repository = DataSourceRepository()

  def create(self, data_source: Source) -> Source:

    ref_id = str(uuid.uuid4().hex)
    db_source_params = []
    for param in data_source.params:
      param_id = str(uuid.uuid4().hex)
      db_param = SourceParameters(name=param.name, value=param.value, ref_id=param_id)
      db_source_params.append(db_param)

    db_source = DataSources(ref_id=ref_id,
                            name=data_source.name,
                            type=data_source.type,
                            desc=data_source.desc,
                            created_at=datetime.now(),
                            updated_at=datetime.now(),
                            created_by=CurrentUser.get_current_user_email(),
                            updated_by=CurrentUser.get_current_user_email(),
                            params=db_source_params)
    try:
      return self.repository.create(db_source)
    except Exception as e:
      logger.error("Unable to create a DataSource {}".format(e))
      raise DataSourceException("Unable to create a DataSource {}".format(e))

  def get_all(self, extends) -> List[Source]:
    try:
      sources = self.repository.get_all()
      data_sources_list = []
      for source in sources:
        data_source = Source.from_orm(source)
        if not extends:
          del data_source.params
        data_sources_list.append(data_source)
    except Exception as e:
      logger.error("Unable to get all DataSources {}".format(e))
      raise DataSourceException("Unable to get all DataSources {}".format(e))
    return data_sources_list

  def get_by_ref_id(self, ref_id: str) -> Source:
    try:
      source = self.repository.get_by_ref_id(ref_id)
    except Exception as e:
      logger.error("No DataSource found with ref_id {}, {}".format(ref_id, e))
      raise DataSourceNotFoundException("No DataSource found with ref_id {}, {}".format(ref_id, e))

    return Source.from_orm(source)

  def get_by_id(self, id: int) -> Source:
    try:
      source = self.repository.get_by_id(id)
    except Exception as e:
      logger.error("No DataSource found with id {}, {}".format(id, e))
      raise DataSourceNotFoundException("No DataSource found with id {}, {}".format(id, e))

    return Source.from_orm(source)

  def update_by_ref_id(self, ref_id: str, data_source: Source) -> Source:
    self.get_by_ref_id(ref_id)
    try:
      source = self.repository.get_by_ref_id(ref_id)
      params_list = []
      for param in data_source.params:
        param_id = str(uuid.uuid4().hex)
        db_param = SourceParameters(name=param.name, value=param.value, ref_id=param_id)
        params_list.append(db_param)

      source.params = params_list

      statement = update(DataSources).values(
          desc=data_source.desc,
          updated_by=CurrentUser.get_current_user_email(),
          updated_at=datetime.now()).where(DataSources.ref_id == ref_id)
      self.repository.update_by_ref_id(statement)

      return self.get_by_ref_id(ref_id)
    except Exception as e:
      logger.error("Unable to update DataSource with ref_id {}, {}".format(ref_id, e))
      raise DataSourceException("Unable to update DataSource with ref_id {}, {}".format(ref_id, e))

  def delete_by_id(self, ref_id: str):
    self.get_by_ref_id(ref_id)
    try:
      self.repository.delete_source(ref_id)
    except Exception as e:
      logger.error("Unable to delete DataSource with ref_id {}, {}".format(ref_id, e))
      raise DataSourceException("Unable to delete DataSource with ref_id {}, {}".format(ref_id, e))

  def get_fields(self, ref_id: str):
    data_source = self.get_by_ref_id(ref_id)
    type = data_source.type
    datasource = Source.from_orm(data_source)
    dict_obj = datasource.dict()
    if type == DataSourceTypeEnum.SALESFORCE:
      return SalesforceDataSource(dict_obj).sf_fields_details()
    if type == DataSourceTypeEnum.SNOWFLAKE:
      return SnowflakeDataSource(dict_obj).sf_fields_details()
