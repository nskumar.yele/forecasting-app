import csv
from io import StringIO

import pandas as pd
from sqlalchemy import update, bindparam

from app.crud.forecast_result_repository import ForecastResultRepository
from app.errors.forecast_result_errors import ForecastException
from app.logger import logger
from app.models.data_base import ForecastingResult
from app.service.model_version_service import ModelVersionService


class ForecastResultService:

  def __init__(self):
    self.repository = ForecastResultRepository()
    self.model_version_service = ModelVersionService()

  def upload_forecast_result(self, model_version_ref_id, file):
    try:
      model_version = self.model_version_service.get_model_version_by_ref_id(model_version_ref_id)
      input_columns = model_version.parameters.input_columns

      for input in input_columns:
        if input.get("forecast_period", None):
          forecast_period = str(input["forecast_period"])

      rows_list = self.__read_csv_file(file)
      df = pd.DataFrame(rows_list[1:], columns=rows_list[0])

      sku_col = df.columns[-1]
      sku_list = df[sku_col].unique().tolist()
      forecast_results = []

      for sku in sku_list:
        index = 0
        sku_df = (df.groupby([sku_col]).get_group(sku))
        sku_df_list = sku_df.values.tolist()
        sku_row_len = len(df.groupby([sku_col]).get_group(sku))
        for row in sku_df_list:
          index += 1
          if (sku_row_len - int(forecast_period)) >= index:
            forecast_result = ForecastingResult(model_version_id=model_version.id,
                                                date=row[0],
                                                actual=row[1],
                                                forecast=0,
                                                sku=row[-1])
          else:
            forecast_result = ForecastingResult(model_version_id=model_version.id,
                                                date=row[0],
                                                forecast=row[1],
                                                actual=0,
                                                sku=row[-1])

          forecast_results.append(forecast_result)

      self.repository.bulk_insert_forecast_result(forecast_results)
    except Exception as e:
      logger.error(
          "Exception raised while inserting Forecast results for model version {}, {}".format(
              model_version_ref_id, e))
      raise ForecastException("Unable to insert Forecast results for model version {}, {}".format(
          model_version_ref_id, e))

  def get_forecast_result(self, model_version_ref_id):
    try:
      model_version = self.model_version_service.get_model_version_by_ref_id(model_version_ref_id)
      return self.repository.get_forecast_result(model_version.id)
    except Exception as e:
      logger.error(
          "Exception raised while getting forecast result from the database with ref_id: {}, {}".
          format(model_version_ref_id, e))
      raise ForecastException(
          "No forecast result found for the given model version with ref_id {}, {}".format(
              model_version_ref_id, e))

  def upload_actual_forecast_result(self, model_version_ref_id, file):
    model_version = self.model_version_service.get_model_version_by_ref_id(model_version_ref_id)
    input_columns = model_version.parameters.input_columns
    target_col = str(model_version.parameters.target_column)

    for input in input_columns:
      if input.get("time_col", None):
        time_col = str(input["time_col"])
      if input.get("sku_col", None):
        sku_col = str(input["sku_col"])

    logger.info("file {}".format(file))

    logger.info("time_col {}".format(time_col))
    logger.info("sku_col {}".format(sku_col))

    try:
      rows_list = self.__read_csv_file(file)
      update_result = []
      for row in rows_list:
        update_result.append({
            "b_date": row[time_col],
            "b_actual": row[target_col],
            "b_filter": 0,
            "b_csku": row[sku_col]
        })
      statement = update(ForecastingResult).values(actual=bindparam("b_actual")).where(
          (ForecastingResult.date == bindparam("b_date"))
          & (ForecastingResult.actual == bindparam("b_filter"))
          & (ForecastingResult.sku == bindparam("b_csku"))
          & (ForecastingResult.model_version_id == model_version.id))
      self.repository.update_actual_result_for_forecast(statement, update_result)
    except Exception as e:
      logger.error("Exception raised while uploading actual data into forecasting-result "
                   "table with ref_id: {}, {}".format(model_version_ref_id, e))
      raise ForecastException(
          "Unable to upload actual data for the model-version  with ref_id {}, {}".format(
              model_version_ref_id, e))

    return self.repository.get_forecast_result(model_version.id)

  def __read_csv_file(self, file):
    decoded = file.decode()
    buffer = StringIO(decoded)
    return list(csv.DictReader(buffer))
