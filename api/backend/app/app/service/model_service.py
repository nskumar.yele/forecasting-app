import json
import uuid
from datetime import datetime
from typing import List
from sqlalchemy import update
from app.crud.model_repository import ModelRepository
from app.errors.model_errors import ModelException, ModelNotFoundException
from app.logger import logger
from app.models.data_base import Model, ModelVersion, ModelParameters
from app.schemas.data_source import Source
from app.schemas.models import ModelVersionsOutput, ModelOutput, ModelInput
from app.service.datasource_service import DataSourceService
from app.service.model_version_service import ModelVersionService
from app.service.workflow_service import WorkflowService
from app.service.notification_service import NotificationService
from app.usecases.template_factory import TemplateFactory
from app.core.auth import CurrentUser


class ModelService:

  def __init__(self):
    self.datasource_service = DataSourceService()
    self.template_factory = TemplateFactory()
    self.workflow_service = WorkflowService()
    self.repository = ModelRepository()
    self.model_version_service = ModelVersionService()
    self.notification_service = NotificationService()

  def create_model(self, model_data: ModelInput, background_tasks) -> ModelOutput:

    data_source: Source = self.datasource_service.get_by_ref_id(model_data.data_source)
    version_no = 1
    model_source = self.__build_model_source(model_data, version_no)
    model_source.data_source_id = data_source.id
    try:
      model: Model = self.repository.create_model(model_source)
      data = {}
      data['type'] = model.type
      data['model_version_id'] = model.versions[0].id
      data['model_version_ref_id'] = model.versions[0].ref_id
      username = model.versions[0].created_by

      background_tasks.add_task(self.generate_work_flow, data_source.ref_id, username, data)
      return self.__parse_model_output(model)
    except Exception as e:
      logger.error("Unable to create the model {}".format(e))
      raise ModelException("Unable to create the model {}".format(e))

  def get_all_models(self, extends) -> List[ModelOutput]:
    try:
      models_list = []
      models = self.repository.get_all_models()
      for item in models:
        if item:
          item_ = item['Model']
          item_.data_source = item['DataSources'].ref_id
          model_dict = ModelOutput.from_orm(item_).dict(exclude={'versions': {'__all__': {'id'}}})
          if not extends:
            model_dict.pop("versions", None)
          models_list.append(model_dict)
      return models_list
    except Exception as e:
      logger.error("Unable to get all Models {}".format(e))
      raise ModelException("Unable to get all Models, {}".format(e))

  def get_model_by_ref_id(self, ref_id: str) -> ModelOutput:
    try:
      model = self.repository.get_model_by_ref_id(ref_id)
      model_output = model[0]['Model']
      model_output.data_source = model[0]['DataSources'].ref_id
      return ModelOutput.from_orm(model_output)
    except Exception as e:
      logger.error("No Model found with ref_id {}, {}".format(ref_id, e))
      raise ModelNotFoundException("No Model found with ref_id {}, {}".format(ref_id, e))

  def update_model_by_ref_id(self, ref_id: str, model_data: ModelInput, background_tasks):
    model = self.repository.get_model_by_ref_id(ref_id)
    model_res = model[0]
    version_no = 1
    if len(model_res['Model'].versions) != 0:
      version_no = len(model_res['Model'].versions) + 1
    datasource_ref_id = model_data.data_source
    model_version_source = self.__update_model_source(model_data, version_no)
    logger.debug("model_Version_Source {}".format(model_version_source))
    model_res['Model'].versions.append(model_version_source)

    data = {}
    data['type'] = model_res['Model'].type
    data['model_version_id'] = model_version_source.id
    data['model_version_ref_id'] = model_version_source.ref_id
    username = model_version_source.created_by
    statement = update(Model).values(desc=model_data.desc,
                                     updated_by=CurrentUser.get_current_user_email(),
                                     updated_at=datetime.now()).where(Model.ref_id == ref_id)
    try:
      self.repository.update_model_by_ref_id(statement)
      model_dict = ModelOutput.from_orm(model_res['Model'])
      model_dict.dict(exclude={'versions'})
      model_dict.versions = [ModelVersionsOutput.from_orm(model_version_source)]
      model_dict.data_source = datasource_ref_id
      background_tasks.add_task(self.generate_work_flow, datasource_ref_id, username, data)
      return model_dict.dict(exclude={'versions': {'__all__': {'id'}}})

    except Exception as e:
      logger.error("Unable to update the model with ref_id {}, {}".format(ref_id, e))
      raise ModelException("Unable to update the model {}, {}".format(ref_id, e))

  def delete_model_by_ref_id(self, ref_id: str):
    self.get_model_by_ref_id(ref_id)
    try:
      self.repository.delete_model_by_ref_id(ref_id)
    except Exception as e:
      logger.error("Unable to delete model with ref_id {}, {}".format(ref_id, e))
      raise ModelException("Unable to delete model with ref_id {}, {}".format(ref_id, e))

  def __build_model_source(self, model_data: ModelInput, version_no) -> Model:
    model_params = []
    model_params_input_source = ModelParameters(ref_id=str(uuid.uuid4().hex),
                                                parameter="input_columns",
                                                value=json.dumps(
                                                    model_data.parameters.input_columns))
    model_params_target_source = ModelParameters(ref_id=str(uuid.uuid4().hex),
                                                 parameter="target_column",
                                                 value=model_data.parameters.target_column)
    model_params.append(model_params_input_source)
    model_params.append(model_params_target_source)
    model_version_source = ModelVersion(id=str(uuid.uuid4().hex),
                                        ref_id=str(uuid.uuid4().hex),
                                        deployment_name="",
                                        workflow_name="",
                                        version=version_no,
                                        created_by=CurrentUser.get_current_user_email(),
                                        created_at=datetime.now(),
                                        updated_at=datetime.now(),
                                        status="CREATED",
                                        updated_by=CurrentUser.get_current_user_email(),
                                        parameters=model_params)
    model_version_list = [model_version_source]
    model_source = Model(id=str(uuid.uuid4().hex),
                         ref_id=str(uuid.uuid4().hex),
                         name=model_data.name,
                         desc=model_data.desc,
                         type=model_data.type,
                         created_by=CurrentUser.get_current_user_email(),
                         created_at=datetime.now(),
                         updated_at=datetime.now(),
                         updated_by=CurrentUser.get_current_user_email(),
                         versions=model_version_list)

    return model_source

  def __update_model_source(self, model_data: ModelInput, version_no) -> Model:
    model_params = []
    model_params_input_source = ModelParameters(ref_id=str(uuid.uuid4().hex),
                                                parameter="input_columns",
                                                value=json.dumps(
                                                    model_data.parameters.input_columns))
    model_params_target_source = ModelParameters(ref_id=str(uuid.uuid4().hex),
                                                 parameter="target_column",
                                                 value=model_data.parameters.target_column)
    model_params.append(model_params_input_source)
    model_params.append(model_params_target_source)
    model_version_source = ModelVersion(id=str(uuid.uuid4().hex),
                                        ref_id=str(uuid.uuid4().hex),
                                        deployment_name="",
                                        workflow_name="",
                                        version=version_no,
                                        created_by=CurrentUser.get_current_user_email(),
                                        created_at=datetime.now(),
                                        updated_at=datetime.now(),
                                        status="CREATED",
                                        updated_by=CurrentUser.get_current_user_email(),
                                        parameters=model_params)

    return model_version_source

  def __parse_model_output(self, data: Model) -> ModelOutput:
    model = ModelOutput.from_orm(data)
    return model

  def generate_work_flow(self, datasource_ref_id: str, username: str, version_data):
    template = self.template_factory.create(version_data["type"])
    model_version = self.model_version_service.get_model_version_by_ref_id(
        version_data["model_version_ref_id"])
    sku = ""
    input_columns = model_version.parameters.input_columns
    for input in input_columns:
      if type(input) == dict:
        if input.get("sku", None):
          sku = str(input["sku"])
    data = {
        "model_version_id": version_data["model_version_id"],
        "model_version_ref_id": version_data["model_version_ref_id"],
        "source_ref_id": datasource_ref_id,
    }
    workflow_spec = template.generate(data=data)
    workflow_response = self.workflow_service.submit(workflow_spec)
    self.model_version_service.update_model_version(version_data["type"],
                                                    version_data["model_version_ref_id"],
                                                    workflow_response["metadata"]["name"])
    if sku:
      parameters = {
          "user": username,
          "message": "FORECAST HAS BEEN CREATED FOR SKU(S) : {}".format(sku),
          "severity": "INFO"
      }
      self.notification_service.create_notification(parameters=parameters,
                                                    destination="PUSH_NOTIFICATION")
