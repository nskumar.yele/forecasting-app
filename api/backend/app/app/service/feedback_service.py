from app.logger import logger
from app.service.notification_service import NotificationService
from app.core.auth import CurrentUser


class FeedbackService:

  def __init__(self):
    self.service = NotificationService()

  def send_feedback_mail(self, feed_back_body, backgroundtasks):
    logger.debug("Inside feedback service method")
    feedback_dict = {
        "feedback": feed_back_body,
        "user": CurrentUser.get_current_user_email(),
        "status": "FEEDBACK",
        "cc": None
    }
    backgroundtasks.add_task(self.service.create_notification,
                             parameters=feedback_dict,
                             destination="EMAIL")
