import os
import sys

from fastapi_mail import ConnectionConfig, MessageSchema, FastMail

from app.logger import logger


class EmailService:

  def __init__(self):
    try:
      from_email = os.environ['SMTP_USER']
      mail_port = os.environ['SMTP_PORT']
      mail_server = os.environ['SMTP_HOST']
      mail_tls = os.environ['SMTP_TLS']
      mail_ssl = os.environ['SMTP_SSL']
      mail_password = os.environ['MAIL_PASSWORD']
      conf = ConnectionConfig(MAIL_USERNAME=from_email,
                              MAIL_FROM=from_email,
                              MAIL_PASSWORD=mail_password,
                              MAIL_PORT=mail_port,
                              MAIL_SERVER=mail_server,
                              MAIL_TLS=mail_tls,
                              MAIL_SSL=mail_ssl)
      self.fast_mail = FastMail(conf)
    except Exception as e:
      logger.error(
          "Exception raised while getting environment variables for sending email: {}".format(e))
      sys.exit(1)

  async def send(self, subject, to_email, template, cc=None):
    try:

      message = MessageSchema(subject=subject, recipients=[to_email], body=template)

      await self.fast_mail.send_message(message)
      logger.info("Successfully sent email to the user {}".format(to_email))
    except Exception as e:
      print("{}".format(e))
