import asyncio
import os
from datetime import datetime
import uuid

from sqlalchemy import update, delete

from app.crud.notification_repository import NotificationRepository
from app.logger import logger
from app.service.email_service import EmailService
from app.models.data_base import Notification

from jinja2 import Environment, FileSystemLoader


class NotificationService:

  def __init__(self):
    self.repository = NotificationRepository()

  def create_notification(self, parameters, destination):
    if destination == "EMAIL":
      self.send_email_service(parameters)
    elif destination == "PUSH_NOTIFICATION":
      self.create_push_notification(parameters)

  def get_template(self, name):
    logger.debug("Loading templates from: %s",
                 os.path.join(os.path.dirname(__file__), '../email_templates/templates'))
    env = Environment(loader=FileSystemLoader(
        os.path.join(os.path.dirname(__file__), '../email_templates/templates')))
    return env.get_template(name)

  def create_template(self, parameters):
    username = parameters.get("user", None).partition(".")[0]
    status = parameters.get("status", None)
    self.template_dict = {}
    data = {
        "username": username,
        "date": datetime.now().date(),
        "sku": parameters.get("sku", None),
        "duration": parameters.get("duration", None),
        "feedback": parameters.get("feedback", None)
    }
    template = self.get_template(status.lower())
    tm = template.render(data=data)
    self.template_dict['subject'] = "Forecast run completed successfully"
    self.template_dict['user_email'] = parameters.get("user", None)
    if status == "ERROR":
      self.template_dict['subject'] = "Forecast run is unsuccessful"
    elif status == "FEEDBACK":
      self.template_dict['subject'] = "CUSTOMER FEEDBACK MAIL"
      self.template_dict['user_email'] = "vinay.kagithapu@predera.com"

    self.template_dict['username'] = username
    self.template_dict['cc'] = parameters.get("cc", None)
    self.template_dict['template'] = tm
    return self.template_dict

  def send_email_service(self, parameters):
    self.template = None
    try:
      logger.info("Received request to send email")
      email_serivce = EmailService()
      self.template = self.create_template(parameters)
      logger.info("Template for email {}".format(self.template))
      asyncio.run(
          email_serivce.send(subject=self.template['subject'],
                             to_email=self.template['user_email'],
                             template=self.template['template'],
                             cc=None))
    except Exception as e:
      logger.error("Unable to send Email {}, {}".format(self.template, e))

  def create_push_notification(self, parameters):
    ref_id = str(uuid.uuid4().hex)
    push_notification = Notification(source="SYSTEM",
                                     user=parameters["user"],
                                     message=parameters["message"],
                                     status="NEW",
                                     severity=parameters["severity"],
                                     ref_id=ref_id,
                                     created_by=parameters["user"],
                                     updated_by=parameters["user"],
                                     created_at=datetime.now(),
                                     updated_at=datetime.now())
    self.repository.create_push_notification(push_notification)

  def get_push_notifications(self, user):
    return self.repository.get_push_notifications(user)

  def update_notifications(self, ref_id, user):
    statement = update(Notification).values(
        status="READ", updated_by=user,
        updated_at=datetime.now()).where((Notification.ref_id.in_(ref_id))
                                         & (Notification.user == user))
    self.repository.update_notifications(statement)

  def delete_notifications(self, ref_id, user):
    statement = delete(Notification).where((Notification.ref_id.in_(ref_id))
                                           & (Notification.user == user))
    self.repository.update_notifications(statement)
