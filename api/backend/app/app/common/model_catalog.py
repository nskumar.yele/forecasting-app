import json
import requests
import sys
import os
from app.common.aiq_service import AiqService
from app.logger import logger


def modelCatalog(model_path):

  try:
    aiq_service = AiqService()
    auth_token = aiq_service.get_aiq_auth_token()

  except Exception as e:
    logger.error("Error occurred while getting auth_token : {}".format(e))
    sys.exit(1)

  # uploading model into model catalog
  url = "https://sandbox.predera.com/aiq/api/model-files/upload"
  header = {"Authorization": 'Bearer ' + auth_token}
  file = {'file': open(model_path, 'rb')}

  file_name = os.path.basename(model_path)

  data = {
      "model_type": "REGRESSION",
      "ml_library": "H2O_AUTOML",
      "model_name": "sales-force-model-new",
      "ml_algorithm": "GBM",
      "project_name": "lead-scoring-solution",
      "project_id": "a-1504733944",
      "filename": file_name
  }

  queryString = {"model_file": json.dumps(data)}

  try:
    response = requests.post(url=url, headers=header, files=file, params=queryString)

  except Exception as e:
    logger.error("Exception raised while uploading model : {}".format(e))

  logger.info(response.status_code)
  upload_response = response.json()

  # Registering model
  register_header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + auth_token}
  register_url = "https://sandbox.predera.com/aiq/api/registered-models"
  register_data = {
      "description": " Sales force leads data",
      "ml_algorithm": "GBM",
      "ml_library": "H2O_AUTOML",
      "model_file_id": upload_response["id"],
      "model_type": "CLASSIFICATION",
      "name": "sales-force-model-three",
      "project_id": "a-1504733944",
      "project_name": "lead-scoring-solution",
      "runtime": "PYTHON3",
      "type": "FILE"
  }

  register_response = requests.post(url=register_url,
                                    data=json.dumps(register_data),
                                    headers=register_header)
  logger.debug("register_response : {}".format(register_response.json()))
  register_id = register_response.json()['id']
  host = "https://sandbox.predera.com"
  api_url = "/aiq/api/registered-model-versions?registered_model_id.equals"
  registered_model_version_url = "{}{}={}".format(host, api_url, register_id)

  # Getting registered model version id
  try:
    new_response = requests.get(url=registered_model_version_url, headers=header)
    registered_model_version_id = new_response.json()[0]['id']
    logger.debug("New_Response : {}".format(new_response.json()))
  except Exception as e:
    logger.error("Error Raised {}".format(e))
  logger.info("Model catalog registered........!!!")
  return registered_model_version_id
