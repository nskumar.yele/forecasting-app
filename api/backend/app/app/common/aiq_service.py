import sys
import requests
import os
from app.logger import logger


class AiqService(object):

  def __init__(self):
    try:
      self.AIQ_URL = os.environ['AIQ_URL']
      self.AUTH_USER = os.environ['AIQ_USERNAME']
      self.AUTH_PASS = os.environ['AIQ_PASSWORD']
    except Exception as e:
      logger.error('Error fetching env variables required for auth: {}'.format(e))
      sys.exit(1)

    try:
      logger.info('Generating token required for '
                  'authentication using auth API')
      credentials = {'userName': self.AUTH_USER, 'password': self.AUTH_PASS}
      self.AUTH_URL = self.AIQ_URL + "/auth-manager/auth"
      r = requests.post(self.AUTH_URL, json=credentials, verify=False)
    except Exception as e:
      logger.error('Error requesting for token using '
                   'AUTH api: {}'.format(e))
      sys.exit(1)

    if r.status_code == 200:
      logger.info('Successfully fetched auth token')
      self.token = r.json()['token']
    else:
      logger.error('Error fetching auth token: {}, '
                   'Response: {}, exiting'.format(r.content, r.status_code))
      sys.exit(1)

  def get_aiq_auth_token(self):
    return self.token
