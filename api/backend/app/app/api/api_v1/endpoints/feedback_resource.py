from fastapi import FastAPI, Response, Request
from app.logger import logger
from app.service.feedback_service import FeedbackService
from app.core.auth import token_required
from fastapi import BackgroundTasks


class FeedbackResource(FastAPI):

  def __init__(self):
    super(FeedbackResource, self).__init__()
    self.service = FeedbackService()

    @self.post("/feedback")
    @token_required
    def send_feedback(request: Request, response: Response, feedback: dict,
                      backgroundtasks: BackgroundTasks):
      logger.info("Received request to post feedback")
      feed_back_body = feedback['feedback']
      logger.debug("inside feed back resource of recieved feedback is {}".format(feed_back_body))
      return self.service.send_feedback_mail(feed_back_body, backgroundtasks)
