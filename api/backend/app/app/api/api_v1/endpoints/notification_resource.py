from fastapi import FastAPI, Response, Request
from app.core.auth import token_required, CurrentUser
from app.logger import logger
from app.service.notification_service import NotificationService
from app.schemas.notifications import Notifications
from typing import List


class NotificationResource(FastAPI):

  def __init__(self):
    super(NotificationResource, self).__init__()
    self.service = NotificationService()

    @self.get("/notifications", response_model=List[Notifications])
    @token_required
    def get_push_notifications(request: Request, response: Response):
      logger.info("Requested to get notifications")
      user = CurrentUser.get_current_user_email()
      return self.service.get_push_notifications(user)

    @self.put("/notifications")
    @token_required
    def update_notifications(request: Request, response: Response, ref_id: List[str]):
      logger.info("Requested to update notifications")
      user = CurrentUser.get_current_user_email()
      self.service.update_notifications(ref_id, user)

    @self.delete("/notifications")
    @token_required
    def delete_notifications(request: Request, response: Response, ref_id: List[str]):
      logger.info("Requested to delete notifications")
      user = CurrentUser.get_current_user_email()
      self.service.delete_notifications(ref_id, user)
      return Response(status_code=200)
