from typing import Union

from fastapi import BackgroundTasks
from fastapi import Request, Response, FastAPI

from app.core.auth import token_required
from app.logger import logger
from app.schemas.models import ModelVersionsOutput
from app.schemas.problem_json import ProblemJson
from app.service.model_version_service import ModelVersionService


class ModelVersionResources(FastAPI):

  def __init__(self):
    super(ModelVersionResources, self).__init__()
    self.service = ModelVersionService()

    @self.get("/model-versions/{ref_id}",
              response_model=Union[ModelVersionsOutput, ProblemJson],
              response_model_exclude={'id'})
    @token_required
    def get_model_version_by_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested to get model version by ref_id {}".format(ref_id))
      return self.service.get_model_version_by_ref_id(ref_id)

    @self.put("/model-versions/{ref_id}")
    async def update_model_version_with_data(request: Request, response: Response, ref_id: str,
                                             background_tasks: BackgroundTasks):
      logger.debug("Requested for update Model Version with ref_id is {}".format(ref_id))
      data = await request.json()
      self.service.update_model_version_with_data(ref_id, data, background_tasks)

    @self.delete("/model-versions/{ref_id}")
    @token_required
    def delete_model_version_by_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested for delete model version with ref_id: {}".format(ref_id))
      self.service.delete_model_version_by_id(ref_id)
      return Response(status_code=200, content='Successfully deleted')
