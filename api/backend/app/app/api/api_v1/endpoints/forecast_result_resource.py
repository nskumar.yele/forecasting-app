from typing import Union, List, Any

from fastapi import UploadFile, File, FastAPI

from app.logger import logger
from app.schemas.models import ForecastResult
from app.schemas.problem_json import ProblemJson
from app.service.forecast_result_service import ForecastResultService


class ForecastResultResources(FastAPI):

  def __init__(self):
    super(ForecastResultResources, self).__init__()
    self.service = ForecastResultService()

    @self.post("/forecast-results")
    async def upload_forecast_result(model_version_ref_id: Union[str, None] = None,
                                     file: UploadFile = File(...)):
      logger.debug(
          "Requested to upload forecast result with model version {}".format(model_version_ref_id))
      contents = await file.read()
      logger.info("file received: {}".format(contents))
      self.service.upload_forecast_result(model_version_ref_id, contents)

    @self.get("/forecast-results/{model_version_ref_id}",
              response_model=Union[List[ForecastResult], Any])
    def get_forecast_result(model_version_ref_id: str):
      logger.debug(
          "Request to get forecast result with model version {}".format(model_version_ref_id))
      return self.service.get_forecast_result(model_version_ref_id)

    @self.put("/forecast-results/{model_version_ref_id}",
              response_model=Union[List[ForecastResult], ProblemJson])
    async def upload_actual_for_forecast(model_version_ref_id: str, file: UploadFile = File(...)):
      logger.debug("Requested to upload actual forecast result with model version {}".format(
          model_version_ref_id))
      contents = await file.read()
      return self.service.upload_actual_forecast_result(model_version_ref_id, contents)
