import json
from typing import Union, Any

from fastapi import FastAPI, Response, BackgroundTasks, Request

from app.core.auth import token_required
from app.logger import logger
from app.schemas.models import ModelInput, ModelOutput
from app.schemas.problem_json import ProblemJson
from app.service.model_service import ModelService


class ModelResources(FastAPI):

  def __init__(self):
    super(ModelResources, self).__init__()
    self.service = ModelService()

    @self.post("/models",
               status_code=201,
               response_model=Union[ModelOutput, ProblemJson],
               response_model_exclude_none=True)
    @token_required
    def create_model(request: Request, response: Response, model: ModelInput,
                     background_tasks: BackgroundTasks):

      logger.debug("Requested to create a model")
      return self.service.create_model(
          model, background_tasks).dict(exclude={'versions': {
              '__all__': {'id'}
          }})

    @self.get("/models", response_model=Union[ModelOutput, Any])
    @token_required
    def get_all_models(request: Request, response: Response):

      logger.debug("Requested to get all models")
      params = request.query_params.get("extends", "False")
      extends = json.loads(params.lower())
      return self.service.get_all_models(extends)

    @self.get("/models/{ref_id}")
    @token_required
    def get_by_ref_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested to get model by ref_id {}".format(ref_id))
      model = self.service.get_model_by_ref_id(ref_id)
      return model.dict(exclude={'versions': {'__all__': {'id', 'parameters'}}})

    @self.put("/models/{ref_id}",
              response_model=Union[ModelOutput, ProblemJson],
              response_model_exclude_none=True)
    @token_required
    def update_by_ref_id(request: Request, response: Response, model: ModelInput, ref_id: str,
                         background_tasks: BackgroundTasks):
      logger.debug("Requested to update model by ref_id {}".format(ref_id))
      return self.service.update_model_by_ref_id(ref_id, model, background_tasks)

    @self.delete("/models/{ref_id}")
    @token_required
    def delete_by_ref_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested to delete model by ref_id {}".format(ref_id))
      self.service.delete_model_by_ref_id(ref_id)
      return Response(status_code=200, content='Successfully deleted')
