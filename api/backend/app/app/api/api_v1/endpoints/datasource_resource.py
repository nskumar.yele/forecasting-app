import json
from typing import List, Union, Any

from fastapi import FastAPI, Response, Request

from app.core.auth import token_required
from app.logger import logger
from app.schemas.data_source import Source
from app.schemas.problem_json import ProblemJson
from app.service.datasource_service import DataSourceService


class DataSourceResources(FastAPI):

  def __init__(self):
    super(DataSourceResources, self).__init__()
    self.service = DataSourceService()

    @self.post("/sources",
               status_code=201,
               response_model=Union[Source, ProblemJson],
               response_model_exclude={'id'})
    @token_required
    def create_sources(request: Request, response: Response, data_source: Source):

      logger.debug("Requested for create DataSource")
      return self.service.create(data_source)

    @self.get("/sources", response_model=Union[List[Source], Any], response_model_exclude={'id'})
    @token_required
    def get_sources(request: Request, response: Response):
      logger.debug("Requested for get all DataSource")
      params = request.query_params.get("extends", "False")
      extends = json.loads(params.lower())
      return self.service.get_all(extends)

    @self.get("/sources/{ref_id}",
              response_model=Union[Source, ProblemJson],
              response_model_exclude={'id'})
    @token_required
    def get_source_by_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested for get DataSource with ref_id is {}".format(ref_id))
      return self.service.get_by_ref_id(ref_id)

    @self.put("/sources/{ref_id}", response_model=Union[Source, Any], response_model_exclude={'id'})
    @token_required
    def update_source_by_id(request: Request, response: Response, ref_id: str, data_source: Source):
      logger.debug("Requested for update DataSource with ref_id is {}".format(ref_id))
      return self.service.update_by_ref_id(ref_id, data_source)

    @self.delete("/sources/{ref_id}")
    @token_required
    def delete_source_by_id(request: Request, response: Response, ref_id: str):
      logger.debug("Requested for delete DataSource with ref_id is {}".format(ref_id))
      self.service.delete_by_id(ref_id)
      return Response(status_code=200, content='Successfully deleted')

    @self.get("/sources/{ref_id}/fields")
    def get_fileds_by_datasource(ref_id: str):
      return self.service.get_fields(ref_id)
