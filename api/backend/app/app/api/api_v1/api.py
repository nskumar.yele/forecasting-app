from fastapi import APIRouter
from app.api.api_v1.endpoints.datasource_resource import DataSourceResources
from app.api.api_v1.endpoints.model_resource import ModelResources
from app.api.api_v1.endpoints.model_version_resource import ModelVersionResources
from app.api.api_v1.endpoints.forecast_result_resource import ForecastResultResources
from app.api.api_v1.endpoints.notification_resource import NotificationResource
from app.api.api_v1.endpoints.feedback_resource import FeedbackResource
from app.api.api_v1.endpoints import items, login, users, utils

api_router = APIRouter()

api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(DataSourceResources().router, prefix="", tags=["datasource"])
api_router.include_router(ModelResources().router, prefix="", tags=["model_resource"])
api_router.include_router(ModelVersionResources().router,
                          prefix="",
                          tags=["model_version_resource"])
api_router.include_router(ForecastResultResources().router, prefix="", tags=["forecast_result"])
api_router.include_router(NotificationResource().router, prefix="", tags=["notification_resource"])
api_router.include_router(FeedbackResource().router, prefix="", tags=["feedback_resource"])
api_router.include_router(utils.router, prefix="/utils", tags=["utils"])
api_router.include_router(items.router, prefix="/items", tags=["items"])
