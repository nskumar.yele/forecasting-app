import json
from datetime import datetime
from enum import Enum
from typing import Optional, List, Any, Union

from pydantic import BaseModel
from pydantic.utils import GetterDict


class AutoName(Enum):

  def _generate_next_value_(self, name, start, count, last_values):
    return name


class LeadModelEnum(AutoName):

  def __str__(self):
    return str(self.value)

  LEADCONVERSION = "Lead Conversion"
  PRODUCTFIT = "Product Fit"
  AGENTROUTING = "Agent Routing"


class ForecastModelEnum(AutoName):

  def __str__(self):
    return str(self.value)

  DEMANDFORECAST = "Demand Forecast"


class ConfigBase(BaseModel):

  class Config:
    orm_mode = True


class ModelOutputParamsGetter(GetterDict):

  def get(self, key: Any, default: Any = None) -> Any:
    if key == "input_columns":
      for column in self._obj:
        if column.parameter == "input_columns":
          return json.loads(column.value)
    elif key == "target_column":
      for column in self._obj:
        if column.parameter == "target_column":
          return column.value

    else:
      return default


class ModelOutputParams(ConfigBase):
  input_columns: List
  target_column: str

  class Config:
    orm_mode = True
    getter_dict = ModelOutputParamsGetter


class ModelInputParams(ConfigBase):
  input_columns: List
  target_column: str


class ModelBase(ConfigBase):
  name: str
  desc: Optional[str]
  type: str


class ModelInput(ModelBase):
  data_source: str
  parameters: ModelInputParams


class ModelVersionsOutput(ConfigBase):
  id: Optional[str]
  ref_id: str
  deployment_name: str
  workflow_name: str
  version: int
  created_by: Optional[str]
  created_at: datetime
  updated_by: str
  updated_at: datetime
  status: str
  status_message: Optional[str]
  performance_metrics: Optional[str]
  feature_significance: Optional[str]
  parameters: ModelOutputParams


class ModelOutput(ModelBase):
  ref_id: str
  created_at: datetime
  updated_at: datetime
  created_by: Optional[str]
  updated_by: Optional[str]
  data_source: Optional[str]
  versions: List[ModelVersionsOutput]


class ForecastResult(ConfigBase):
  date: datetime
  actual: Union[int, None]
  forecast: Union[int, None]
  sku: int


class WorkflowData(BaseModel):
  model_version_id: str
  model_version_ref_id: str
  source_ref_id: str
