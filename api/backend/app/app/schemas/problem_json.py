from pydantic import BaseModel


class ProblemJson(BaseModel):
  title: str
  status: int
  detail: str
