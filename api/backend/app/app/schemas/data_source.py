from datetime import datetime
from enum import Enum
from typing import Optional, List

from pydantic import BaseModel


class DataSourceTypeEnum(Enum):

  def __str__(self):
    return str(self.value)

  BIGCOMMERCE = "BIGCOMMERCE"
  SHOPIFY = "SHOPIFY"
  SALESFORCE = "SALESFORCE"
  AIQ_DATASET = "AIQ_DATASET"
  DYNAMICS365 = "DYNAMICS365"
  SNOWFLAKE = "SNOWFLAKE"


class DataSourceBaseModel(BaseModel):

  class Config:
    orm_mode = True


class DataSourceParams(DataSourceBaseModel):
  name: str
  value: str
  ref_id: Optional[str]


class Source(DataSourceBaseModel):
  id: Optional[int]
  name: str
  type: DataSourceTypeEnum
  desc: Optional[str]
  ref_id: Optional[str]
  created_at: Optional[datetime]
  updated_at: Optional[datetime]
  created_by: Optional[str]
  updated_by: Optional[str]
  params: List[DataSourceParams]
