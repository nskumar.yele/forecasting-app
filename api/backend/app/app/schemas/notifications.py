from pydantic import BaseModel
from datetime import datetime


class ConfigBase(BaseModel):

  class Config:
    orm_mode = True


class Notifications(ConfigBase):
  ref_id: str
  source: str
  user: str
  message: str
  severity: str
  status: str
  created_at: datetime
  created_by: str
  updated_at: datetime
  updated_by: str
