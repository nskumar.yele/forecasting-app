import uuid
from datetime import datetime
from unittest import mock

from fastapi.testclient import TestClient

from app.main import app
from app.schemas.data_source import Source, DataSourceParams
from app.models.data_base import Model, ModelVersion, ModelParameters
from app.schemas.models import ModelOutput, ModelVersionsOutput, ModelOutputParams
from app.common.aiq_service import AiqService


def get_access_token():
  aiq_service = AiqService()
  return aiq_service.get_aiq_auth_token()


def prepare_response_model_version():
  params = ModelOutputParams(input_columns=['test_user1', 'test-user2', 'test-user3'],
                             target_column="target")
  return ModelVersionsOutput(id=1,
                             ref_id=str(uuid.uuid4().hex),
                             deployment_name="test-modelVersion",
                             workflow_name="AIQ_WORKFLOW",
                             version=1,
                             created_by="test-user1",
                             created_at=datetime.now(),
                             updated_by="test-user1",
                             updated_at=datetime.now(),
                             status="running",
                             status_message="Done",
                             performance_metrics="performance",
                             feature_significance="feature-significance",
                             parameters=params)


def prepare_response():
  params = ModelOutputParams(input_columns=[{
      "time_col": "processing_date"
  }, {
      "sku_col": "SKU_ID"
  }, {
      "sales_col": "sales"
  }, {
      "timestamp_format": "%Y-%m-%d"
  }, {
      "forecast_period": 2
  }, {
      "data_horizon": "daily"
  }, {
      "sku": "132135"
  }],
                             target_column="sales")

  model_version_out = [
      ModelVersionsOutput(id=1,
                          ref_id=str(uuid.uuid4().hex),
                          deployment_name="test-modelVersion",
                          workflow_name="AIQ_WORKFLOW",
                          version=1,
                          created_by="test-user1",
                          created_at=datetime.now(),
                          updated_by="test-user1",
                          updated_at=datetime.now(),
                          status="running",
                          status_message="Success",
                          performance_metrics="performance",
                          feature_significance="feature-significance",
                          parameters=params)
  ]

  return ModelOutput(name="test-user",
                     desc="test-purpose",
                     type="Demand Forecast",
                     ref_id=str(uuid.uuid4().hex),
                     created_at=datetime.now(),
                     updated_at=datetime.now(),
                     created_by="test-user1",
                     updated_by="test-user1",
                     data_source="8552260eff224a2b8613c563d17f7cb8",
                     versions=model_version_out)


def prepare_response_extends_false():
  return {
      "name": "test-user",
      "desc": "test-purpose",
      "type": "AIQ_DATASET",
      "ref_id": str(uuid.uuid4().hex),
      "created_at": datetime.now(),
      "updated_at": datetime.now(),
      "created_by": "test-user1",
      "updated_by": "test-user1",
      "data_source": "AIQ_DATASET"
  }


def prepare_response_ref_id():
  params = [
      DataSourceParams(name="username", value="testuser", ref_id=str(uuid.uuid4().hex)),
      DataSourceParams(name="password", value="testpassword", ref_id=str(uuid.uuid4().hex))
  ]

  return Source(id=1,
                ref_id=str(uuid.uuid4().hex),
                name="test-dataSource",
                type='AIQ_DATASET',
                created_by="test_user1",
                updated_by="test_user1",
                desc="test dataSource creation",
                created_at=datetime.now(),
                updated_at=datetime.now(),
                params=params)


def prepare_response_data_source():
  params = [
      DataSourceParams(name="username", value="testuser", ref_id=str(uuid.uuid4().hex)),
      DataSourceParams(name="password", value="testpassword", ref_id=str(uuid.uuid4().hex))
  ]
  return Source(id=1,
                ref_id=str(uuid.uuid4().hex),
                name="test-dataSource",
                type="AIQ_DATASET",
                desc="test dataSource creation",
                created_at=datetime.now(),
                updated_at=datetime.now(),
                params=params)


def prepare_response_create():
  ref_id = str(uuid.uuid4().hex)
  model_params = [
      ModelParameters(
          model_version_id=ref_id,
          parameter="input_columns",
          value='[{"time_col": "date"}, {"sku_col": "CSKU_ID"}, {"timestamp_format": "%Y-%m-%d"},'
          '{"forecast_period": 5}, {"data_frequency": "weekly"}, {"forecast_horizon": "W"}]'),
      ModelParameters(model_version_id=ref_id, parameter="target_column", value="sales")
  ]

  model_version = [
      ModelVersion(id=ref_id,
                   ref_id=str(uuid.uuid4().hex),
                   model_id="model_id",
                   deployment_name="test-modelVersion",
                   workflow_name="AIQ_WORKFLOW",
                   version=1,
                   created_by="test-user1",
                   created_at=datetime.now(),
                   updated_by="test-user1",
                   updated_at=datetime.now(),
                   status="running",
                   status_message="Success",
                   performance_metrics="performance",
                   feature_significance="feature-significance",
                   parameters=model_params)
  ]

  return Model(id="1",
               created_at=datetime.now(),
               created_by="test-user1",
               updated_by="test-user1",
               updated_at=datetime.now(),
               ref_id=str(uuid.uuid4().hex),
               data_source_id=1,
               name="test-dataSource",
               type="Demand Forecast",
               desc="test dataSource creation",
               versions=model_version)


@mock.patch("app.service.model_service.ModelService.generate_work_flow")
@mock.patch("app.crud.model_repository.ModelRepository.create_model")
@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_by_ref_id")
@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
def test_create_model(mock_get_model_version, mock_get_by_ref, mock_create_model,
                      mock_generate_workflow):
  params = {
      "name": "test-model-run-02",
      "type": "Demand Forecast",
      "desc": "test-run-5",
      "parameters": {
          "input_columns": [{
              "time_col": "date"
          }, {
              "sku_col": "CSKU_ID"
          }, {
              "timestamp_format": "%Y-%m-%d"
          }, {
              "forecast_period": 5
          }, {
              "data_frequency": "weekly"
          }, {
              "forecast_horizon": "W"
          }, {
              "sku":
              "85914318,81194046,87949942,41206320, 61608398,67303970,79945176,"
              "80718484,38892900,64021986,120066646,120210338,120355334,120356328,120485898"
          }],
          "target_column":
          "sales"
      },
      "data_source": "f61fdd845b8d4dcfa1739776dfeefeb3"
  }
  data_response = prepare_response_data_source()
  data_response.ref_id = str(uuid.uuid4().hex)
  response_model_version = prepare_response_model_version()
  mock_get_model_version.return_value = response_model_version
  mock_get_by_ref.return_value = data_response
  mock_create_model.return_value = prepare_response_create()
  mock_generate_workflow.return_value = None

  response = TestClient(app).post(
      "/api/v1/models",
      json=params,
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response.status_code == 201


@mock.patch("app.crud.model_repository.ModelRepository.get_all_models")
def test_getparams_all_models(mock_get_all_models):
  mock_get_all_models.return_value = [{
      "Model": prepare_response_create(),
      "DataSources": prepare_response_data_source()
  }]

  response = TestClient(app).get(
      "/api/v1/models/?extends=True",
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  # print(get_access_token())
  assert response.status_code == 200
  res = response.json()
  print(res)
  assert len(res[0]['versions']) != 0


@mock.patch("app.crud.model_repository.ModelRepository.get_all_models")
def test_get_all_models_with_extends_false(mock_get_all_models):
  mock_get_all_models.return_value = [{
      "Model": prepare_response_create(),
      "DataSources": prepare_response_data_source()
  }]
  response = TestClient(app).get(
      "/api/v1/models?extends=False",
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response.status_code == 200
  res = response.json()
  assert "versions" not in res[0].keys()


@mock.patch("app.crud.model_repository.ModelRepository.get_model_by_ref_id")
def test_get_by_ref_id(mock_get_model_by_ref_id):
  ref_id = str(uuid.uuid4().hex)
  response = []
  di = {}
  di['Model'] = prepare_response()
  di['DataSources'] = prepare_response_ref_id()

  response.append(di)

  mock_get_model_by_ref_id.return_value = [response[0]]
  response[0]['Model'].ref_id = ref_id
  response_api = TestClient(app).get(
      "/api/v1/models/{}".format(ref_id),
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response_api.status_code == 200
  res = response_api.json()
  print(res['versions'][0])
  assert 'parameters' not in res['versions'][0]
  assert res['ref_id'] == ref_id


@mock.patch("app.service.model_service.ModelService.generate_work_flow")
@mock.patch("app.crud.model_repository.ModelRepository.get_model_by_ref_id")
@mock.patch("app.crud.model_repository.ModelRepository.update_model_by_ref_id")
@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
def test_update_by_ref_id(mock_get_model_version, mock_update_model_by_ref_id,
                          mock_get_model_by_ref_id, mock_generate_workflow):
  ref_id = str(uuid.uuid4().hex)
  params = {
      "name": "test-user",
      "desc": "test-purpose",
      "type": "Demand Forecast",
      "data_source": "8552260eff224a2b8613c563d17f7cb8",
      "parameters": {
          "input_columns": [{
              "time_col": "processing_date"
          }, {
              "sku_col": "SKU_ID"
          }, {
              "sales_col": "sales"
          }, {
              "timestamp_format": "%Y-%m-%d"
          }, {
              "forecast_period": 2
          }, {
              "data_horizon": "daily"
          }, {
              "sku": "132135"
          }],
          "target_column":
          "target"
      }
  }

  response = []
  di = {}
  di['Model'] = prepare_response()
  di['DataSources'] = prepare_response_ref_id()

  response.append(di)
  response_model_version = prepare_response_model_version()
  mock_get_model_version.return_value = response_model_version
  mock_get_model_by_ref_id.return_value = [response[0]]
  response[0]['Model'].ref_id = ref_id
  mock_update_model_by_ref_id.return_value = prepare_response()
  mock_generate_workflow.return_value = None

  response_api = TestClient(app).put(
      "/api/v1/models/{}".format(ref_id),
      json=params,
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response_api.status_code == 200
  res = response_api.json()
  assert res['desc'] == "test-purpose"


@mock.patch("app.crud.model_repository.ModelRepository.get_model_by_ref_id")
@mock.patch("app.crud.model_repository.ModelRepository.delete_model_by_ref_id")
def test_delete_by_ref_id(mock_delete_model_by_ref_id, mock_get_model_by_ref_id):
  ref_id = str(uuid.uuid4().hex)
  response = []
  di = {}
  di['Model'] = prepare_response()
  di['DataSources'] = prepare_response_ref_id()

  response.append(di)

  mock_get_model_by_ref_id.return_value = [response[0]]
  response[0]['Model'].ref_id = ref_id
  mock_delete_model_by_ref_id.return_value = None
  response_api = TestClient(app).delete(
      "/api/v1/models/{}".format(ref_id),
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response_api.status_code == 200
