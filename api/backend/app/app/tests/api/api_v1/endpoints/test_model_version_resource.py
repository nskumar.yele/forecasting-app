import uuid
from datetime import datetime
from unittest import mock

from fastapi.testclient import TestClient

from app.main import app
from app.schemas.models import ModelVersionsOutput, ModelOutputParams
from app.common.aiq_service import AiqService


def get_access_token():
  aiq_service = AiqService()
  return aiq_service.get_aiq_auth_token()


def prepare_response():
  params = ModelOutputParams(input_columns=[{
      "time_col": "date"
  }, {
      "sku_col": "CSKU_ID"
  }, {
      "timestamp_format": "%Y-%m-%d"
  }, {
      "forecast_period": 5
  }],
                             target_column="target")
  return ModelVersionsOutput(id=1,
                             ref_id=str(uuid.uuid4().hex),
                             deployment_name="test-modelVersion",
                             workflow_name="AIQ_WORKFLOW",
                             version=1,
                             created_by="test-user1",
                             created_at=datetime.now(),
                             updated_by="test-user1",
                             updated_at=datetime.now(),
                             status="running",
                             status_message="Done",
                             performance_metrics="performance",
                             feature_significance="feature-significance",
                             parameters=params)


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
def test_get_model_version_by_id(mock_repository):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response()
  response.ref_id = ref_id
  mock_repository.return_value = response
  res = TestClient(app).get("/api/v1/model-versions/{}".format(ref_id),
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
  assert res.json()['ref_id'] == ref_id


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.update_model_version")
def test_update_model_version_with_data(mock_repo_update, mock_repository_get_version):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response()
  response.ref_id = ref_id
  mock_repository_get_version.return_value = response
  mock_repo_update.return_value = None
  params = {"metrics": "mapescore", "feature_significance": "test dataSource creation"}
  res = TestClient(app).put("/api/v1/model-versions/{}".format(ref_id),
                            json=params,
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.delete_model_version_by_id")
def test_delete_model_version_by_id(mock_repo_delete_model_version, mock_repo_get_version):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response()
  response.ref_id = ref_id
  mock_repo_get_version.return_value = response
  mock_repo_delete_model_version.return_value = None
  res = TestClient(app).delete(
      "/api/v1/model-versions/{}".format(ref_id),
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
