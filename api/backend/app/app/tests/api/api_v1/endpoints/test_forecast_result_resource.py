import uuid
from datetime import datetime
from unittest import mock

from fastapi.testclient import TestClient
from app.schemas.models import ModelVersionsOutput, ModelOutputParams, ForecastResult
from app.main import app

from app.common.aiq_service import AiqService


def get_access_token():
  aiq_service = AiqService()
  return aiq_service.get_aiq_auth_token()


def prepare_response_forecast():
  params = ForecastResult(date=datetime.now(), actual=2, forecast=2, sku=1)
  return params


def prepare_response_model_ref_id():
  params = ModelOutputParams(input_columns=[{
      "time_col": "date"
  }, {
      "sku_col": "CSKU_ID"
  }, {
      "timestamp_format": "%Y-%m-%d"
  }, {
      "forecast_period": 5
  }],
                             target_column="sales")
  return ModelVersionsOutput(id=1,
                             ref_id=str(uuid.uuid4().hex),
                             deployment_name="test-modelVersion",
                             workflow_name="AIQ_WORKFLOW",
                             version=1,
                             created_by="test-user1",
                             created_at=datetime.now(),
                             updated_by="test-user1",
                             updated_at=datetime.now(),
                             status="running",
                             status_message="Done",
                             performance_metrics="performance",
                             feature_significance="feature-significance",
                             parameters=params)


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
@mock.patch(
    "app.crud.forecast_result_repository.ForecastResultRepository.bulk_insert_forecast_result")
def test_upload_forecast_result(mock_bulk_insert_forecast_result, mock_get_model_ref_id):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response_model_ref_id()
  response.ref_id = ref_id
  mock_get_model_ref_id.return_value = response
  mock_bulk_insert_forecast_result.return_value = None
  file_path = "./some_name.csv"

  data = {"model_version_ref_id": ref_id, "forecast_period": 2}
  res = TestClient(app).post(
      "/api/v1/forecast-results",
      params=data,
      files={"file": open(file_path, 'r')},
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
@mock.patch("app.crud.forecast_result_repository.ForecastResultRepository.get_forecast_result")
def test_get_forecast_result(mock_forecast_result, mock_get_model_ref_id):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response_model_ref_id()
  response.ref_id = ref_id
  mock_get_model_ref_id.return_value = response
  mock_forecast_result.return_value = [prepare_response_forecast()]
  res = TestClient(app).get("/api/v1/forecast-results/{}".format(ref_id),
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200


@mock.patch("app.crud.model_version_repository.ModelVersionRepositry.get_model_version_by_ref_id")
@mock.patch(
    "app.crud.forecast_result_repository.ForecastResultRepository.update_actual_result_for_forecast"
)
@mock.patch("app.crud.forecast_result_repository.ForecastResultRepository.get_forecast_result")
def test_upload_actual_for_forecast(mock_forecast_result, mock_update_actual_forecast_result,
                                    mock_get_model_ref_id):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response_model_ref_id()
  response.ref_id = ref_id
  mock_forecast_result.return_value = [prepare_response_forecast()]
  mock_get_model_ref_id.return_value = response
  mock_update_actual_forecast_result.return_value = [prepare_response_forecast()]
  file_path = "./upload_actual.csv"
  res = TestClient(app).put("/api/v1/forecast-results/{}".format(ref_id),
                            files={"file": open(file_path, 'r')},
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
