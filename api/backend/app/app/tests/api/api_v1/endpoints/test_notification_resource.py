import uuid
from datetime import datetime
from unittest import mock

from fastapi.testclient import TestClient
from app.models.data_base import Notification
from app.main import app
from app.schemas.notifications import Notifications
from app.common.aiq_service import AiqService


def get_access_token():
  aiq_service = AiqService()
  return aiq_service.get_aiq_auth_token()


def prepare_response():
  params = Notifications(ref_id=str(uuid.uuid4().hex),
                         source="data_Source",
                         user="test_user1",
                         message="message",
                         severity="severity",
                         status="running",
                         created_at=datetime.now(),
                         created_by="test_user1",
                         updated_at=datetime.now(),
                         updated_by="test-uesr1")
  return params


def prepare_response_update():
  params = Notification(id="one",
                        ref_id=str(uuid.uuid4().hex),
                        user="test_user1",
                        message="message",
                        status="running",
                        severity="severity",
                        source="data_Source")
  return params


@mock.patch("app.crud.notification_repository.NotificationRepository.get_push_notifications")
def test_get_push_notifications(mock_push_noti):
  mock_push_noti.return_value = [prepare_response()]
  response = TestClient(app).get(
      "/api/v1/notifications",
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  print(response.json())
  assert response.status_code == 200


@mock.patch("app.crud.notification_repository.NotificationRepository.update_notifications")
def test_update_notifications(update_noti):
  ref_id = str(uuid.uuid4().hex)
  update_noti.return_value = prepare_response_update
  response = TestClient(app).put(
      "/api/v1/notifications",
      json=[ref_id],
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  print(response.json())
  assert response.status_code == 200


@mock.patch("app.crud.notification_repository.NotificationRepository.update_notifications")
def test_delete_notifications(delete_noti):
  ref_id = str(uuid.uuid4().hex)
  delete_noti.return_value = None
  response = TestClient(app).delete(
      "/api/v1/notifications",
      json=[ref_id],
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response.status_code == 200
