import uuid
from datetime import datetime
from unittest import mock

from fastapi.testclient import TestClient

from app.main import app
from app.schemas.data_source import Source, DataSourceParams
from app.common.aiq_service import AiqService


def get_access_token():
  aiq_service = AiqService()
  return aiq_service.get_aiq_auth_token()


def prepare_response():
  params = [
      DataSourceParams(name="username", value="testuser", ref_id=str(uuid.uuid4().hex)),
      DataSourceParams(name="password", value="testpassword", ref_id=str(uuid.uuid4().hex))
  ]
  return Source(id=1,
                ref_id=str(uuid.uuid4().hex),
                name="test-dataSource",
                type="AIQ_DATASET",
                desc="test dataSource creation",
                created_at=datetime.now(),
                updated_at=datetime.now(),
                params=params)


@mock.patch("app.crud.datasource_repository.DataSourceRepository.create")
def test_create_sources_success(mock_repository):
  params = [{
      "name": "username",
      "value": "testuser"
  }, {
      "name": "password",
      "value": "testpassword"
  }]
  request = {
      "name": "test-dataSource",
      "type": "AIQ_DATASET",
      "desc": "test dataSource creation",
      "params": params
  }
  mock_repository.return_value = prepare_response()

  response = TestClient(app).post(
      "/api/v1/sources",
      json=request,
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})

  assert response.status_code == 201


@mock.patch("app.crud.datasource_repository.DataSourceRepository.create")
def test_create_sources_invalid_request(mock_repository):
  params = [{
      "name": "username",
      "value": "testuser"
  }, {
      "name": "password",
      "value": "testpassword"
  }]
  request = {"type": "AIQ_DATASET", "desc": "test dataSource creation", "params": params}
  mock_repository.return_value = prepare_response()
  response = TestClient(app).post(
      "/api/v1/sources",
      json=request,
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response.status_code == 422


@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_all")
def test_get_sources(mock_repository):
  mock_repository.return_value = [prepare_response()]
  response = TestClient(app).get(
      "/api/v1/sources", headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  print(response.json())
  assert response.status_code == 200
  assert len(response.json()) == 1


@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_all")
def test_get_sources_empty(mock_repository):
  mock_repository.return_value = []
  response = TestClient(app).get(
      "/api/v1/sources", headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert response.status_code == 200
  assert len(response.json()) == 0


@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_by_ref_id")
def test_get_sources_by_ref_id(mock_repository):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response()
  response.ref_id = ref_id
  mock_repository.return_value = response
  res = TestClient(app).get("/api/v1/sources/" + ref_id,
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
  assert res.json()['ref_id'] == ref_id


@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_by_ref_id")
@mock.patch("app.crud.datasource_repository.DataSourceRepository.update_by_ref_id")
def test_update_sources_by_ref_id(mock_repo_update, mock_repository_by_ref_id):
  ref_id_update = str(uuid.uuid4().hex)
  desc_update = "test dataSource creation update"
  params = [{
      "name": "username",
      "value": "testuser"
  }, {
      "name": "password",
      "value": "testpassword"
  }]
  request = {
      "name": "test-dataSource",
      "type": "AIQ_DATASET",
      "desc": desc_update,
      "params": params
  }
  response = prepare_response()
  response.desc = desc_update
  mock_repository_by_ref_id.return_value = response
  mock_repo_update.return_value = response
  res = TestClient(app).put("/api/v1/sources/" + ref_id_update,
                            json=request,
                            headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
  assert res.json()['desc'] == desc_update


@mock.patch("app.crud.datasource_repository.DataSourceRepository.get_by_ref_id")
@mock.patch("app.crud.datasource_repository.DataSourceRepository.delete_source")
def test_delete_source_by_ref_id(mock_repository_delete, mock_repository_get):
  ref_id = str(uuid.uuid4().hex)
  response = prepare_response()
  response.ref_id = ref_id
  mock_repository_get.return_value = response
  mock_repository_delete.return_value = None
  res = TestClient(app).delete(
      "/api/v1/sources/{}".format(ref_id),
      headers={"authorization": "{}{}".format('Bearer ', get_access_token())})
  assert res.status_code == 200
