from typing import List, Union

from app.db.session import session
from app.models.data_base import Model, DataSources


class ModelRepository:

  def __init__(self):
    self.session = session

  def create_model(self, data: Model) -> Model:

    self.session.add(data)
    self.session.commit()
    return data

  def get_all_models(self) -> List[Union[Model, DataSources]]:
    return self.session.query(Model, DataSources).where(DataSources.id == Model.data_source_id)

  def get_model_by_ref_id(self, ref_id: str):
    return self.session.query(
        Model,
        DataSources).where(DataSources.id == Model.data_source_id).filter_by(ref_id=ref_id).all()

  def update_model_by_ref_id(self, statement):
    self.session.execute(statement)
    self.session.commit()

  def delete_model_by_ref_id(self, ref_id: str):
    obj = self.session.query(Model).filter_by(ref_id=ref_id).one()
    self.session.delete(obj)
    self.session.commit()
