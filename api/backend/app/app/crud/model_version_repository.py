from app.db.session import session
from app.models.data_base import ModelVersion


class ModelVersionRepositry:

  def __init__(self):
    self.session = session

  def get_model_version_by_ref_id(self, ref_id: str):
    return self.session.query(ModelVersion).filter_by(ref_id=ref_id).one()

  def update_model_version(self, statement):
    self.session.execute(statement)
    self.session.commit()

  def delete_model_version_by_id(self, ref_id: str):
    model_version_delete_obj = self.session.query(ModelVersion).filter_by(ref_id=ref_id).one()
    self.session.delete(model_version_delete_obj)
    self.session.commit()
