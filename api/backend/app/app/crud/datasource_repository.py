from typing import List

from app.db.session import session
from app.models.data_base import DataSources
from app.schemas.data_source import Source


class DataSourceRepository:

  def __init__(self):
    self.session = session

  def create(self, data: DataSources) -> Source:
    self.session.add(data)
    self.session.commit()
    return Source.from_orm(data)

  def get_all(self) -> List[Source]:
    return self.session.query(DataSources).all()

  def get_by_ref_id(self, ref_id: str) -> Source:
    return self.session.query(DataSources).filter_by(ref_id=ref_id).one()

  def get_by_id(self, id: int):
    return self.session.query(DataSources).filter_by(id=id).one()

  def update_by_ref_id(self, statement):
    self.session.execute(statement)
    self.session.commit()

  def delete_source(self, ref_id: str):
    obj = self.session.query(DataSources).filter_by(ref_id=ref_id).one()
    self.session.delete(obj)
    self.session.commit()
