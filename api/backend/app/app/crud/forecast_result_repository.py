from typing import List

from app.db.session import session
from app.models.data_base import ForecastingResult


class ForecastResultRepository:

  def __init__(self):
    self.session = session

  def bulk_insert_forecast_result(self, forecast_results: List[ForecastingResult]):
    self.session.add_all(forecast_results)
    self.session.commit()

  def get_forecast_result(self, model_version_id):
    return self.session.query(ForecastingResult).filter_by(
        model_version_id=model_version_id).order_by(ForecastingResult.id).all()

  def update_actual_result_for_forecast(self, statement, data):
    self.session.execute(statement, data)
    self.session.commit()
