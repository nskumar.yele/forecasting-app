from app.models.data_base import Notification
from app.db.session import session
from app.schemas.notifications import Notifications
from typing import List


class NotificationRepository:

  def __init__(self):
    self.session = session

  def create_push_notification(self, data: Notification):
    self.session.add(data)
    self.session.commit()

  def get_push_notifications(self, user) -> List[Notifications]:
    return self.session.query(Notification).where(Notification.user == user).all()

  def update_notifications(self, statement):
    self.session.execute(statement)
    self.session.commit()

  def delete_notifications(self, statement):
    self.session.execute(statement)
    self.session.commit()
