import json
import sys

import requests
from app.run.usecases.services.aiq_service import AiqService
from app.run.usecases.util.logger import logger


class ApiClient:

  def __init__(self, model_version_ref_id, source_ref_id):
    self.model_version_ref_id = model_version_ref_id
    self.source_ref_id = source_ref_id
    self.url = "http://forecasting-api.forecasting-solution.svc.cluster.local/api/v1"
    # self.url = "https://d5d3-103-125-161-120.in.ngrok.io/api/v1"

  def get_source(self):
    aiq_service = AiqService()
    token = aiq_service.get_aiq_auth_token()
    bearer_token = 'Bearer ' + token

    url = self.url
    try:
      response = requests.get(url=url + "/sources/" + self.source_ref_id,
                              headers={'Authorization': bearer_token})
    except Exception as e:
      logger.error("Unable to get response from sources api : {}".format(e))
      logger.debug("Calling update model-version api to update the status")
      data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
      self.update_model_version(data)
      sys.exit(1)

    try:
      datasource = response.json()
      logger.debug("Source object received {}".format(datasource))
    except Exception as e:
      logger.error("Unable to parse the response into JSON {}".format(e))
      logger.info("Calling update model-version api to update the status")
      data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
      self.update_model_version(data)
      sys.exit(1)

    return datasource

  def get_model_version(self):
    aiq_service = AiqService()
    token = aiq_service.get_aiq_auth_token()
    bearer_token = 'Bearer ' + token
    url = self.url
    try:
      response = requests.get(url=url + "/model-versions/" + self.model_version_ref_id,
                              headers={'Authorization': bearer_token})
    except Exception as e:
      logger.error("Unable to get response from Model-Version api : {}".format(e))
      logger.info("Calling update model-version api to update the status")
      data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
      self.update_model_version(data)
      sys.exit(1)
    try:
      model_version = response.json()
      logger.debug("Model_Version object received {}".format(model_version))
    except Exception as e:
      logger.error("Unable to parse the response into JSON {}".format(str(e)))
      logger.info("Calling update model-version api to update the status")
      data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
      self.update_model_version(data)
      sys.exit(1)
    return model_version

  def update_model_version(self, data):
    aiq_service = AiqService()
    token = aiq_service.get_aiq_auth_token()
    bearer_token = 'Bearer ' + token
    url = self.url

    try:
      response = requests.put(url=url + "/model-versions/" + self.model_version_ref_id,
                              headers={'Authorization': bearer_token},
                              data=json.dumps(data))

      logger.info("Successfully sent data to update model-version, status code is {}".format(
          response.status_code))
      response.raise_for_status()
    except requests.exceptions.HTTPError as e:
      logger.error("Exception raised while sending data: {}".format(str(e)))
      sys.exit(1)

  def send_forecast_result_to_api(self, data_file, data):
    aiq_service = AiqService()
    token = aiq_service.get_aiq_auth_token()
    bearer_token = 'Bearer ' + token
    url = self.url
    try:
      response = requests.post(url=url + "/forecast-results",
                               files=data_file,
                               params=data,
                               headers={'Authorization': bearer_token})

      logger.info("Successfully uploaded the result file, status code is {}".format(
          response.status_code))

      response.raise_for_status()
    except requests.exceptions.HTTPError as e:
      logger.error("Exception raised while sending result file: {}".format(str(e)))
      logger.info("Calling update model-version api to update the status")
      data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
      self.update_model_version(data)
      sys.exit(1)
