import requests
from app.run.usecases.util.logger import logger
from app.run.usecases.services.aiq_service import AiqService


class AiqDatasetSource:

  def __init__(self, datasource):
    self.datasource = datasource
    self.aiq_service = AiqService()
    try:
      for param in datasource["params"]:
        if param["name"] == "dataset_id":
          self.dataset_id = param['value']
    except Exception as e:
      logger.error("Error occurred while parsing the datasource : {}".format(e))

  def get_data(self):
    dataset_id = self.dataset_id
    auth_token = self.aiq_service.get_aiq_auth_token()
    try:
      headers = {'Authorization': "Bearer {}".format(auth_token)}
      data_set_url = "https://sandbox.predera.com/aiq/api/datasets/{}".format(dataset_id)
      api_response = requests.get(url=data_set_url, headers=headers)
      api_response.raise_for_status()
      logger.info("Data set: {}".format(api_response.json()))
    except Exception as e:
      logger.error("Exception raised while getting data set: {}".format(e))

  def download_data(self):
    path = "/tmp/generic_data.csv"
    # path = "/home/vijaykumarguntreddy/results/generic_data.csv"]
    dataset_id = self.dataset_id
    auth_token = self.aiq_service.get_aiq_auth_token()
    try:
      headers = {'Authorization': "Bearer {}".format(auth_token)}
      download_url = "https://sandbox.predera.com/aiq/api/datasets/{}/download".format(dataset_id)
      api_response = requests.get(url=download_url, headers=headers)
      api_response.raise_for_status()
      logger.info("Response status code:{}".format(api_response.status_code))
      with open(path, 'wb') as fd:
        for chunk in api_response.iter_content(chunk_size=128):
          fd.write(chunk)
      return path
    except Exception as e:
      logger.error("Exception raised while downloading data set: {}".format(e))

  def get_fields(self):
    logger.info("aiq data set get_fields api called")

    try:
      url = "https://sandbox.predera.com/aiq/api/datasets/{}/sample-records".format(self.dataset_id)
      header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + self.auth_token}
      logger.info("AIQ samples record function calling")
      response = requests.get(url=url, headers=header)
      logger.info("AIQ samples record function called")
      result = response.json()
      logger.info("Result of sample records function : {}".format(result))
      columns = result[0]
      values = result[1]
      fields = []
      for each_index in range(len(values)):
        obj = {
            "name": columns[each_index],
            "type": type(values[each_index]).__name__,
            "defaultValue": values[each_index]
        }
        fields.append(obj)
      logger.info("AIQ fields are {}".format(fields))
    except Exception as e:
      logger.error("Exception raised t get_fields function , error {}".foramt(e))

    return fields
