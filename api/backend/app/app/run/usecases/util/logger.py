import os
import logging
import datetime

LS_LOGGER_LEVEL = "DEBUG"

if os.environ.get('LS_LOGGER_LEVEL') is not None:
  LS_LOGGER_LEVEL = os.environ['LS_LOGGER_LEVEL']

logger = logging.getLogger(__name__)
logger.setLevel(LS_LOGGER_LEVEL)
logger.propagate = False
console = logging.StreamHandler()
console.setFormatter(
    logging.Formatter("PREDERA-LS {} %(levelname)s: %(message)s".format(
        datetime.datetime.now().isoformat())))
logger.addHandler(console)
