import os
import sys
from app.run.usecases.lead_scoring.h2o_automl import H2O_AutoMl
from app.run.usecases.lead_scoring.model_catalog import modelCatalog
from app.run.usecases.lead_scoring.deployment import deployment_function
from app.run.usecases.util.logger import logger
from app.run.usecases.lead_scoring.data_prep.source_factory import DataSource
from app.run.usecases.services.api_client import ApiClient

model_version_id = os.getenv('MODEL_VERSION_ID')
model_version_ref_id = os.getenv('MODEL_VERSION_REF_ID')
source_ref_id = os.getenv('SOURCE_REF_ID')

logger.info("Model_Version_Id: {}".format(model_version_id))
logger.info("Model_Version_Ref_Id: {}".format(model_version_ref_id))
logger.info("Source_Ref_Id: {}".format(source_ref_id))

api_client = ApiClient(model_version_ref_id, source_ref_id)
datasource = api_client.get_source()
logger.debug("Response from data base for datasource {}".format(datasource))

model_version = api_client.get_model_version()

target_column = model_version["parameters"]["target_column"]

set_source_instance = DataSource(datasource, model_version)
path = set_source_instance.set_source().download_data()

# sending CSV file to h2o cluster and getting model out of it
logger.info("sending CSV file to h2o cluster and getting model out of it")
h2o_automl_instance = H2O_AutoMl(path, target_column)
model_path = h2o_automl_instance.automl()
data = h2o_automl_instance.metrics()

if model_path == "HTTP 500 Server Error":
  sys.exit(1)

# Importing model file into model catalog
logger.info("Importing model file into model catalog")
registered_model_version_id = modelCatalog(model_path)

# deploying
logger.info("Deploying the model")
deployment_function(registered_model_version_id, model_version_id)

logger.debug("Sending metrics to api")

api_client.update_model_version(data)
