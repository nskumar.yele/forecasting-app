import json
import requests
from app.common.aiq_service import AiqService
import sys
from app.run.usecases.util.logger import logger

data = {
    "name": "create-deployment",
    "number_of_runs": 0,
    "run": False,
    "schedule": {},
    "spec": {
        "project_id": "a-1504733944",
        "project_name": "lead-scoring-solution",
        "model_name": "",
        "deploy": {
            "env": [{
                "name": "MODEL_ID",
                "value": ""
            }],
            "env_source": [{
                "name": "lead-scoring-solution-aiq-secrets",
                "type": "secretRef"
            }],
            "name": "deploy-custom-code",
            "replicas": "1",
            "source": {
                "git": {
                    "directory": "runtime/python",
                    "repository":
                    "https://gitlab.com/predera/crm-solutions/lead-scoring-prototype.git",
                    "revision": "feature/lead_score_api",
                    "secret": {
                        "name": "lead-scoring-solution-git-secrets"
                    }
                },
                "image": "python:3.8.13",
                "runtime": "PYTHON3",
                "type": "GIT"
            },
            "sub_type": "DEPLOY_SIMPLE",
            "type": "DEPLOY"
        }
    },
    "metadata": {
        "generateName": "create-deployment-",
        "labels": {}
    }
}


def deployment_function(version_id, model_version_id):
  try:
    aiq_service = AiqService()
    auth_token = aiq_service.get_aiq_auth_token()

  except Exception as e:
    logger.error("Error occurred while getting auth_token : {}".format(e))
    sys.exit(1)

  deployment_url = "https://sandbox.predera.com/aiq/api/mlworkflow-runs"
  header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + auth_token}

  # model_id = model_id.replace("-", "")
  data["spec"]["deploy"]["env"][0]["value"] = version_id
  data["spec"]["model_name"] = model_version_id

  try:
    resposne = requests.post(url=deployment_url, headers=header, data=json.dumps(data))
  except Exception as e:
    logger.error("exception Error : {}".format(e))

  logger.debug(resposne.json())
  logger.info("deployed successfully........!!!!!")
