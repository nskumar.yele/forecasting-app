from app.run.usecases.lead_scoring.data_prep.factory import DataSourceInterface
import pandas as pd
import sys
from app.run.usecases.util.logger import logger
import json
import snowflake.connector
import datetime


class SnowflakeDataSource(DataSourceInterface):

  def __init__(self, datasource=None, model_version=None):
    self.datasource = datasource
    self.model_version = model_version
    try:
      for param in self.datasource['params']:
        if param['name'] == "username":
          snow_username = param["value"]
        if param['name'] == "password":
          snow_password = param["value"]
        if param['name'] == "account":
          snow_account = param["value"]
        if param['name'] == "warehouse":
          snow_warehouse = param["value"]
        if param['name'] == "database":
          snow_db = param["value"]
        if param['name'] == "schema":
          snow_schema = param["value"]
        if param['name'] == "sandbox":
          snow_sandbox = param["value"]

      if snow_sandbox == "False":
        logger.info("sf_sandbox:False")
        self.sf_conn = snowflake.connector.connect(user=snow_username,
                                                   password=snow_password,
                                                   account=snow_account,
                                                   warehouse=snow_warehouse,
                                                   database=snow_db,
                                                   schema=snow_schema,
                                                   quote_identifiers=True)

      else:
        logger.info("sf_sandbox: True")
        self.sf_conn = snowflake.connector.connect(user=snow_username,
                                                   password=snow_password,
                                                   account=snow_account,
                                                   warehouse=snow_warehouse,
                                                   database=snow_db,
                                                   schema=snow_schema,
                                                   quote_identifiers=True)

    except Exception as e:
      logger.error("Error Occurred while logging into snowflake account :{}".format(e))
      sys.exit(1)

  def get_data(self):
    print("Hello from get snowflake")

  def download_data(self):

    def sf_leads_data(input_columns):
      today = datetime.date.today()
      today_str = str(today) + " 00:00:00.000 +0000"
      prev_date = today - datetime.timedelta(days=90)
      prev_date_str = str(prev_date) + " 00:00:00.000 +0000"
      col_names = ", ".join(['\"' + col + '\"' for col in input_columns])
      SOQL = """select {} from LEAD_SCORING_SF WHERE "CreatedDate" > '{}'
      and "CreatedDate" < '{}' """.format(col_names, prev_date_str, today_str)
      logger.debug("Snowflake Query.... {}".format(SOQL))
      df = pd.read_sql_query(SOQL, self.sf_conn)
      logger.debug(df)
      path = "/tmp/sales_force_leads_data.csv"
      df.to_csv(path, index=False)  # /tmp/sales_force_leads_data
      logger.debug("Saved data from Salesforce to {}".format(path))
      logger.info("sales force leads data downloaded successfully ........!!!!!")
      return path

    target_column = self.model_version["parameters"]["target_column"]
    input_column = self.model_version["parameters"]["input_columns"]
    logger.info("input_column: {}".format(input_column))
    logger.info("target_column: {}".format(target_column))
    logger.info("input_column type is : {}".format(type(input_column)))
    input_columns = json.loads(input_column)
    logger.info("input_columns type: {}".format(type(input_columns)))
    return sf_leads_data(input_columns)

  def sf_fields_details(self):
    query = """ select * from LEAD_SCORING_SF """
    logger.debug("Snowflake Query.... {}".format(query))
    df = pd.read_sql_query(query, self.sf_conn)
    field_names = list(df.columns)
    default_values = list(df[0:1].iloc[0])
    values_li = []
    for i in default_values:
      values_li.append(str(type(i)))

    final_dict = {"name": field_names, "type": values_li, "defaultValue": default_values}
    final_li = []
    final_li.append(final_dict)
    return final_li
