from app.run.usecases.lead_scoring.data_prep.sales_force_data_source import SalesforceDataSource
from app.run.usecases.lead_scoring.data_prep.snow_flake_data_source import SnowflakeDataSource
from app.run.usecases.services.aiq_data_set_source import AiqDatasetSource
from app.run.usecases.lead_scoring.data_prep.dynamics_365_datasource import Dynamics365DataSource
from app.run.usecases.util.logger import logger


class SourceFactory:

  def __init__(self, datasource, model_version):
    self.datasource = datasource
    self.model_version = model_version

  def create_instance(self):
    source_type = self.datasource["type"]
    if source_type == 'SALESFORCE':
      return SalesforceDataSource(self.datasource, self.model_version)
    elif source_type == 'DYNAMICS365':
      return Dynamics365DataSource(self.datasource)
    elif source_type == 'AIQ_DATASET':
      return AiqDatasetSource(self.datasource)
    elif source_type == 'SNOWFLAKE':
      return SnowflakeDataSource(self.datasource)

    else:
      raise ValueError("Invalid source type")

  def instance_for_fields(self):
    source_type = self.datasource["type"]
    logger.info("source_type: {}".format(source_type))
    if source_type == 'SALESFORCE':
      salesforce_instance = SalesforceDataSource(self.datasource)
      return salesforce_instance.sf_fields_details()
    elif source_type == 'DYNAMICS365':
      d365_instance = Dynamics365DataSource(self.datasource)
      return d365_instance.get_fields()
    elif source_type == 'AIQ_DATASET':
      aiq_dataset_instance = AiqDatasetSource(self.datasource)
      return aiq_dataset_instance.get_fields()
    if source_type == 'SNOWFLAKE':
      snowflake_instance = SnowflakeDataSource(self.datasource)
      return snowflake_instance.sf_fields_details()
    else:
      raise ValueError("Invalid source type")


class DataSource:

  def __init__(self, datasource, model_version):
    self.datasource = datasource
    self.model_version = model_version

  def set_source(self):
    source_fact = SourceFactory(self.datasource, self.model_version)
    return source_fact.create_instance()

  def get_fields(self):
    source_fact = SourceFactory(self.datasource)
    return source_fact.instance_for_fields()
