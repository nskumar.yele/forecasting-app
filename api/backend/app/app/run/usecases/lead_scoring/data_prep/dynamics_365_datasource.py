import sys
import pandas as pd
import requests
from app.run.usecases.util.logger import logger
from msal import PublicClientApplication


class Dynamics365DataSource:

  def __init__(self, datasource):
    self.username = ""
    self.password = ""
    self.org_url = ""
    self.client_id = ""
    self.tenant_id = ""
    self.datasource = datasource
    try:
      for param in datasource['params']:
        if param["name"] == "username":
          self.username = param["value"]
        if param['name'] == "password":
          self.password = param["value"]
        if param['name'] == "url":
          self.org_url = param["value"]
        if param["name"] == "client_id":
          self.client_id = param["value"]
        if param['name'] == "tenant_id":
          self.tenant_id = param["value"]

      logger.info("type of tenant_id: {}".format(type(self.tenant_id)))
      logger.info("tenant_id: {}".format(self.tenant_id))
      # username = os.environ["DYNAMICS365_USERNAME"]
      # password = os.environ["DYNAMICS365_PASSWORD"]
      # username = "d-nazeer@digipropel.com"
      # password = "DigiPred!"

      # client_id = "51f81489-12ee-4a9e-aaae-a2591f45987d"
      # tenant_id = "3cd6cc04-a0f3-4b1c-aff4-b8bb8c0394ec"
      # self.org_url = "https://org64c3562c.crm8.dynamics.com"
      authority_url = "https://login.microsoftonline.com/{}".format(self.tenant_id)
      app = PublicClientApplication(self.client_id, client_credential=None, authority=authority_url)
      result = app.acquire_token_by_username_password(self.username,
                                                      self.password,
                                                      scopes=["{}/.default".format(self.org_url)],
                                                      claims_challenge=None)

      if "access_token" in result:
        self.access_token = result["access_token"]
        logger.info("access_token: {}".format(self.access_token))
      else:
        logger.error(result.get("error"))
        logger.error(result.get("error_description"))
        logger.error(result.get("correlation_id"))

    except Exception as e:
      logger.error(
          "Exception raised while getting auth token from dynamic365, error : {}".format(e))
      sys.exit(1)

  def get_data(self):
    logger.info("Hello from get dynamic365")

  def download_data(self):
    leads_data = self.d365_data()
    data = pd.DataFrame(leads_data)
    # path = "/home/vijaykumarguntreddy/results/dynamic_data_new.csv"
    path = "/tmp/dynamics365_lead_data.csv"
    data.to_csv(path, index=False)
    logger.info("Dynamics365 data downloaded successfully !!!!")
    return path

  def d365_data(self):
    auth_token = self.access_token
    url = "{}/api/data/v9.0/leads".format(self.org_url)
    header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + auth_token}

    try:
      response = requests.get(url=url, headers=header)
      leads_data = response.json()["value"]

    except Exception as e:
      logger.error(
          "Exception raised while getting leads data from dynamic365, error : {}".format(e))
      sys.exit(1)
    return leads_data

  def get_fields(self):
    # leads_data = self.d365_data()
    auth_token = self.access_token
    url = "{}/api/data/v9.0/leads?$top=1".format(self.org_url)
    header = {"Content-Type": "application/json", "Authorization": 'Bearer ' + auth_token}

    try:
      response = requests.get(url=url, headers=header)
      leads_data = response.json()["value"]
      logger.info("lead_data: {}".format(leads_data))

    except Exception as e:
      logger.error(
          "Exception raised while getting leads data from dynamic365, error : {}".format(e))
      sys.exit(1)
    leads_record = leads_data[0]
    fields = []

    for key, value in leads_record.items():
      if type(value).__name__ == 'NoneType':
        data_type = 'Unknown'
      else:
        data_type = type(value).__name__
      obj = {"name": key, "type": data_type, "defaultValue": value}
      fields.append(obj)

    logger.info("Fields = {}".format(fields))
    return fields
