from simple_salesforce import Salesforce
from app.run.usecases.lead_scoring.data_prep.factory import DataSourceInterface
import pandas as pd
import sys
from app.run.usecases.util.logger import logger


class SalesforceDataSource(DataSourceInterface):

  def __init__(self, datasource=None, model_version=None):
    self.datasource = datasource
    self.model_version = model_version
    try:
      for param in self.datasource['params']:
        if param['name'] == "username":
          sf_username = param["value"]
        if param['name'] == "password":
          sf_password = param["value"]
        if param['name'] == "security_token":
          sf_security_token = param["value"]
        if param['name'] == "sandbox":
          sf_sandbox = param["value"]

      logger.info("Sandbox flag : {}".format(sf_sandbox))
      logger.info("username: {}".format(sf_username))
      logger.info("password: {}".format((sf_password)))
      logger.info("security_token: {}".format(sf_security_token))

      if sf_sandbox == "False":
        logger.info("sf_sandbox:False")
        self.sf = Salesforce(username=sf_username,
                             password=sf_password,
                             security_token=sf_security_token,
                             domain='test')

      else:
        logger.info("sf_sandbox:True")
        self.sf = Salesforce(username=sf_username,
                             password=sf_password,
                             security_token=sf_security_token)

    except Exception as e:
      logger.error("Error Occurred while logging into sales force account :{}".format(e))
      sys.exit(1)

  def get_data(self):
    print("Hello from get salesforce")

  def download_data(self):

    def sf_leads_data(input_columns):

      SOQL = "SELECT {} FROM Lead WHERE CreatedDate = LAST_90_DAYS".format(input_columns)
      logger.debug("Sales force Query.... {}".format(SOQL))

      query_data = self.QueryData(SOQL, self.sf)
      df = pd.DataFrame(query_data)
      logger.debug(df)
      # path = "/tmp/sales_force_leads_data"  # Give path to store csv file on your local
      path = "/tmp/sales_force_leads_data.csv"
      df.to_csv(path, index=False)  # /tmp/sales_force_leads_data
      logger.debug("Saved data from Salesforce to {}".format(path))
      logger.info("sales force leads data downloaded successfully ........!!!!!")
      return path

    target_column = self.model_version["parameters"]["target_column"]
    input_column = self.model_version["parameters"]["input_columns"]
    logger.info("input_column: {}".format(input_column))
    logger.info("target_column: {}".format(target_column))
    logger.info("input_column type is : {}".format(type(input_column)))
    logger.info("input_columns type: {}".format(type(input_column)))
    input_column.append(target_column)
    # columns = []
    # for column in input_columns:
    #   columns.append(column)
    #
    # columns.append(target_column)
    # joined_columns = ','.join(columns)

    col_names = ",".join(input_column)

    return sf_leads_data(col_names)

  def QueryData(self, SOQL, sf):
    qryResult = sf.query(SOQL)
    logger.debug('Total records: ' + str(qryResult['totalSize']))
    data = []
    isDone = qryResult['done']

    if isDone:
      for rec in qryResult['records']:
        rec.pop('attribute', None)
        data.append(rec)
      return data
    while not isDone:
      try:
        if not qryResult['done']:
          for rec in qryResult['records']:
            rec.pop('attribute', None)
            data.append(rec)
          qryResult = sf.query_more(qryResult['nextRecordsUrl'], True)
        else:
          for rec in qryResult['records']:
            rec.pop('attribute', None)
            isDone = True
          logger.info('Query Complete')

      except NameError:
        for rec in qryResult['records']:
          rec.pop('attribute', None)
          data.append(rec)
        qryResult = sf.query_more(qryResult['nextRecordsUrl'], True)
    logger.info('done')
    return data

  def sf_fields_details(self):
    logger.debug("Request to get Lead object fields")
    desc = self.sf.Lead.describe()
    field_names = [field['name'] for field in desc['fields']]
    field_types = [field['type'] for field in desc['fields']]

    SOQL = "SELECT {} FROM Lead limit 1".format(','.join(field_names))
    logger.debug("Sales force Query.... {}".format(SOQL))
    rs = self.sf.query(SOQL)
    firstRec = rs['records'][0]
    field_values = [firstRec[field] for field in firstRec]

    logger.info("field_names {}: {}".format(len(field_names), field_names))
    logger.info("field_types {}: {}".format(len(field_types), field_types))
    logger.info("field_values {}: {}".format(len(field_values), field_values))

    results = []
    for field in desc['fields']:
      results.append({
          'name': field['name'],
          'type': field['type'],
          'defaultValue': firstRec[field['name']]
      })
    logger.info("Salesforce fields data ............ : {}".format(results))

    return results

  def sf_leads_details(self):
    desc = self.sf.Lead.describe()
    field_names = [field['name'] for field in desc['fields']]
    SOQL = "SELECT {} FROM Lead limit 3000".format(','.join(field_names))
    logger.debug("Sales force Query.... {}".format(SOQL))
    query_data = self.QueryData(SOQL, self.sf)
    df = pd.DataFrame(query_data).to_json(orient="table")
    logger.info("Sales force leads data : {}".format(df))

    return df
