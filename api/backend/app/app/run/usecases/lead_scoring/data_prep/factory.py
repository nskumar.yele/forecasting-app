from abc import ABCMeta, abstractmethod


class DataSourceInterface(metaclass=ABCMeta):

  @staticmethod
  @abstractmethod
  def get_data():
    """Interface method - get data"""

  def download_data(self):
    """"Interface method - download data"""
