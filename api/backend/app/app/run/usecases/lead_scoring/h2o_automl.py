import logging
from app.logger import logger
import csv
import pandas as pd
import h2o


class H2O_AutoMl:

  def __init__(self, filepath, target):
    logger.debug("Creating H2O_AutoML using filepath: {}, target:{}".format(filepath, target))
    self.filepath = filepath
    self.target = target
    self.metrics_data = None

  def automl(self):
    filename = self.filepath
    logger.debug("Reading CSV from file: {}".format(filename))
    df = pd.read_csv(filename)
    logger.info("data_frame: {}".format(df))
    # df["IsConverted"][0] = True
    # df["IsConverted"][1] = True

    h2o.init(ip="h2o-h2o-3.h2o-system.svc.cluster.local", port=54321)

    # convert pandas DataFrame into H2O Frame
    train_df = h2o.H2OFrame(df)

    logger.info("train_df: {}".format(train_df))

    # Describe the train h20Frame
    logger.debug("Describe the train h2oFrame")
    train_df.describe()

    test = pd.read_csv(filename)
    test = h2o.H2OFrame(test)
    # selecting feature and label columns
    logger.info("test: {}".format(test))
    logging.debug("getting columns names....")
    x = test.columns
    logger.info("test_columns: {}".format(test.columns))

    logging.debug("Columns in the input file: {}".format(x))
    y = self.target
    logging.debug("Target column set to {}".format(y))
    # remove label classvariable from feature variable
    x.remove(y)

    from h2o.automl import H2OAutoML
    # callh20automl  function

    max_runtime = 200
    aml = H2OAutoML(
        max_runtime_secs=max_runtime,
        # exclude_algos =['DeepLearning'],
        seed=1,
        # stopping_metric='logloss',
        # sort_metric='logloss',
        balance_classes=False)

    try:
      # train model and record time % time
      aml.train(x=x, y=y, training_frame=train_df)
    except Exception as e:
      logger.info("error : {}".format(str(e)))
      logger.info("error_status_code: {}".format(e))
      return e

    # View the H2O aml leaderboard
    lb = aml.leaderboard
    logging.info("View the H2O aml leaderboard : {}".format(lb))

    metrics = lb.head(rows=lb.nrows)

    metrics_df = metrics.as_data_frame()

    path = "/tmp/generic_data.csv"

    metrics_df.to_csv(path, index=False)

    logger.debug("File generic_data.csv is created")

    rows = []
    with open(path, 'r') as csvfile:
      # creating a csv reader object

      csvreader = csv.reader(csvfile)
      fields = next(csvreader)

      for row in csvreader:
        rows.append(row)

    csvfile.close()

    leader_model = rows[0]
    metrics = {}
    field_len = len(fields)

    for i in range(field_len - 1):
      metrics[fields[i + 1]] = rows[0][i + 1]

    for row in rows:
      if "StackedEnsemble" not in row[0]:
        model_not_ensemble = row[0]
        break
    logger.debug("model without StackedEnsemble: {}".format(model_not_ensemble))
    logger.debug("leader_model: {}".format(leader_model))
    logger.debug("metrics_derived: {}".format(metrics))

    var_model = h2o.get_model(model_not_ensemble)

    varimp = var_model.varimp(use_pandas=True)

    logger.debug("varimp csv:{}".format(varimp))

    path = "/tmp/generic_data.csv"

    varimp.to_csv(path, index=False)

    logger.debug("File generic_data.csv is created")

    feature_significance = []
    with open(path, 'r') as csvfile:
      # creating a csv reader object
      csvreader = csv.reader(csvfile)
      fields = next(csvreader)
      for row in csvreader:
        dict = {}
        dict["name"] = row[0]
        dict["significance"] = row[2]
        feature_significance.append(dict)

    logger.debug("feature_significance: {}".format(feature_significance))

    data = {}
    data["metrics"] = metrics
    data["feature_significance"] = feature_significance
    logger.debug("data object which contains the metrics and feature_significance: {}".format(data))
    self.metrics_data = data

    logger.info("All H2O trained Models: ")
    logger.debug(lb.head(rows=lb.nrows))

    # Get the top model of leaderboard
    se = aml.leader
    # logger.debug("Top Model of the Leaderboard : {}".format(se))

    # h2o.download_model(se, path="~/results")                   #to download locally

    model_path = h2o.download_model(se, path="/tmp/H2OModel")
    logger.info("Model Downloaded successfully ..........!!!!!")
    return model_path

  def metrics(self):
    data = self.metrics_data
    return data
