class inputs_checking:

  def __init__(self, data, target_col, date_col, sku_col):
    self.data = data
    self.target_col = target_col
    self.date_col = date_col
    self.sku_col = sku_col

  @staticmethod
  def is_equally_spaced(arr):
    diff = arr[1] - arr[0]
    for x in range(1, len(arr) - 1):
      if arr[x + 1] - arr[x] != diff:
        return False
    return True

  def check_date_col(self):
    temp_df = self.data
    dt = temp_df[self.date_col]
    date_dtype_col_n = len(temp_df.select_dtypes(include="datetime64").columns)
    if date_dtype_col_n != 1:
      print("Data have more than ONE datetime columns which is not the correct data form",
            "Not able to run with this data",
            sep="\n")
      assert date_dtype_col_n == 1, "More Than One Datetime Columns"
    interval = self.is_equally_spaced(dt)
    return interval

  def check_target_col(self):
    temp_df = self.data
    negative_val_n = temp_df[temp_df[self.target_col] < 0].shape[0]
    if negative_val_n > 0:
      print("{} negative {} values were found and all are replced by 0".format(
          negative_val_n, self.target_col))
      temp_df.loc[temp_df[self.target_col] < 0, self.target_col] = 0
    return temp_df

  def __str__(self):
    return "check, are the inputs provided by the user correct or not"
