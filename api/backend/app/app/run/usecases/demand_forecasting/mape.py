import pandas as pd
import numpy as np
from prophet import Prophet

from app.run.usecases.demand_forecasting.validation import inputs_checking
from sklearn.model_selection import train_test_split


def mape_point(actual, pred):
  if actual != 0:
    return 100 * (np.abs(actual - pred) / np.abs(actual))
  else:
    return np.nan


def mape2(y_true, y_pred):
  return np.abs(np.sum(y_true) - np.sum(y_pred)) / np.sum(y_true)


def complication_col(data):
  total_obs = data.shape[0]
  col_missing_val = []
  col_to_remove = []
  for col in data.columns:
    if data[col].isnull().any() and col != "y" and col != "ds":
      non_nan_values = data[col].count()
      if non_nan_values < 0.3 * total_obs:
        col_to_remove.append(col)
      else:
        col_missing_val.append(col)
  return col_missing_val, col_to_remove


def missing_val_imputation(col, data, validation_start_date):
  df_prophet = data[['ds', col]]
  df_prophet.rename(columns={col: 'y'}, inplace=True)
  train_df_fb = df_prophet[df_prophet.ds < validation_start_date]
  model_fb = Prophet()
  model_fb.fit(train_df_fb)
  future = df_prophet[['ds']]
  forecast = model_fb.predict(future)
  return forecast[['yhat']]


def univarite(data, validation_start_date):
  train_df = data[data.ds < validation_start_date]
  test_df = data[data.ds >= validation_start_date]
  model = Prophet()
  model.fit(train_df)
  future = test_df[['ds']]
  forecast = model.predict(future)
  forecast = forecast.yhat.values
  mape = mape2(test_df.y.values, forecast) * 100
  return mape


def multivarite(data, validation_start_date):
  col_missing_val, col_to_remove = complication_col(data)
  col_no_missing_val = set(data.columns).difference(col_missing_val)
  col_no_missing_val = col_no_missing_val.difference(col_to_remove)
  col_no_missing_val = col_no_missing_val.difference(["y", "ds"])
  data.drop(col_to_remove, axis=1, inplace=True)
  data.reset_index(drop=True, inplace=True)
  for col in col_missing_val:
    f = missing_val_imputation(col, data, validation_start_date)
    index = data[col].index[data[col].isnull()]
    c_n = data.columns.get_loc(col)
    for i in index:
      data.iloc[i, c_n] = f.iloc[i, 0]
  data.reset_index(drop=True, inplace=True)
  train_df = data[data.ds < validation_start_date]
  test_df = data[data.ds >= validation_start_date]
  model = Prophet()
  temp_col = ["y", "ds"]
  for col in data.columns:
    if col not in temp_col:
      model.add_regressor(col, standardize=False)
  model.fit(train_df)
  future = test_df.drop(['y'], axis=1)
  forecast = model.predict(future)
  forecast = forecast.yhat.values
  mape = mape2(test_df.y.values, forecast) * 100
  return mape


def start_validation(data_csv, target_col, time_col, sku_col, timestamp_format):
  # setting parameters value in the required format
  df_skus = data_csv.copy()
  df_skus[time_col] = pd.to_datetime(df_skus[time_col], format=timestamp_format)
  for c in df_skus.columns:
    if df_skus[c].dtypes == 'object' and c != sku_col:
      df_skus = df_skus.loc[:, ~df_skus.columns.isin([c])]
  data_checking = inputs_checking(df_skus, target_col, time_col, sku_col)
  data_checking.check_date_col()
  final_df = data_checking.check_target_col()
  df = final_df.copy()
  df.rename(columns={target_col: "y", time_col: "ds"}, inplace=True)
  skus = df[sku_col].unique()
  # start validation
  mape_list = []
  if len(df.columns) == 3:
    for sku in skus:
      print("SKU : ", sku)
      if df[sku_col].dtypes == "int":
        sku = int(sku)
      temp_df = df.groupby([sku_col]).get_group(sku)
      train_df, test_df = train_test_split(temp_df, test_size=0.2, shuffle=False)
      validation_start_dt = train_df["ds"].iloc[-1]
      sku_df = temp_df.copy()
      sku_df.drop([sku_col], axis=1, inplace=True)
      mape_val = univarite(sku_df, validation_start_dt)
      mape_list.append(mape_val)
  else:
    for sku in skus:
      print("SKU : ", sku)
      if df[sku_col].dtypes == "int":
        sku = int(sku)
      temp_df = df.groupby([sku_col]).get_group(sku)
      train_df, test_df = train_test_split(temp_df, test_size=0.2, shuffle=False)
      validation_start_dt = train_df["ds"].iloc[-1]
      sku_df = temp_df.copy()
      sku_df.drop([sku_col], axis=1, inplace=True)
      mape_val = multivarite(sku_df, validation_start_dt)
      mape_list.append(mape_val)
  mape_df = pd.DataFrame()
  mape_df[sku_col] = skus
  mape_df['mape'] = mape_list
  return mape_df
