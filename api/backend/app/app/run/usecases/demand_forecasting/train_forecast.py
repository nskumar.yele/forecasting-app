import pandas as pd
import numpy as np
from prophet import Prophet
import pickle
from app.run.usecases.demand_forecasting.validation import inputs_checking
from app.run.usecases.util.logger import logger
import boto3
import json


def upload_pickle_file_s3(p_object, filename):
  s3_file = "config_s3.json"
  path = s3_file
  with open(path, "r") as s3_config_file:
    s3_inputs = json.loads(s3_config_file.read())
  Access_Key_ID = s3_inputs["Access_Key_ID"]
  Secret_Key = s3_inputs["Secret_Key"]
  client = boto3.client("s3", aws_access_key_id=Access_Key_ID, aws_secret_access_key=Secret_Key)
  BUCKET = "pt-sales-forecasting"
  byte_obj = pickle.dumps(p_object)
  client.put_object(Bucket=BUCKET, Key=filename, Body=byte_obj)


def get_frequency(data, sku_col, skus):
  temp_df = data.groupby([sku_col]).get_group(skus[0])
  temp_df.reset_index(drop=True, inplace=True)
  sorted_datetime = temp_df['ds'].sort_values()
  time_delta = sorted_datetime[1] - sorted_datetime[0]
  if time_delta < pd.Timedelta(24, unit="h"):
    return "h"
  elif time_delta < pd.Timedelta(7, unit="d"):
    return "d"
  elif time_delta == pd.Timedelta(7, unit="d"):
    return "w"
  elif time_delta == pd.Timedelta(30, unit="d") or time_delta == pd.Timedelta(31, unit="d"):
    return "m"
  elif time_delta == pd.Timedelta(365, unit="d") or time_delta == pd.Timedelta(366, unit="d"):
    return "Y"
  else:
    assert False, "datetime is not correctly interval"


def mape_point(actual, pred):
  if actual != 0:
    return 100 * np.abs(actual - pred) / np.abs(actual)
  else:
    return np.nan


def mape2(y_true, y_pred):
  return np.abs(np.sum(y_true) - np.sum(y_pred)) / np.sum(y_true)


def complication_col(data):
  total_obs = data.shape[0]
  col_missing_val = []
  col_to_remove = []
  for col in data.columns:
    if data[col].isnull().any() and col != "y" and col != "ds":
      non_nan_values = data[col].count()
      if non_nan_values < 0.3 * total_obs:
        col_to_remove.append(col)
      else:
        col_missing_val.append(col)
  return col_missing_val, col_to_remove


def missing_val_imputation(sku, col, data, skus_model):
  df_prophet = data[['ds', col]]
  df_prophet.rename(columns={col: 'y'}, inplace=True)
  train_df_fb = df_prophet
  model_fb = Prophet()
  model_fb.fit(train_df_fb)
  skus_model[sku][col] = model_fb
  future = df_prophet[['ds']]
  forecast = model_fb.predict(future)
  return forecast[['yhat']]


def forecast_missing_val_imputation(sku, col, data, skus_model):
  df_prophet = data[['ds', col]]
  df_prophet.rename(columns={col: 'y'}, inplace=True)
  train_df_fb = df_prophet
  model_fb = Prophet()
  model_fb.fit(train_df_fb)
  skus_model[sku][col] = model_fb


def univarite(sku, data, frequency, skus_model, forecast_period, forecast_horizon):
  train_df = data
  model = Prophet()
  model.fit(train_df)
  skus_model[sku] = model
  future = model.make_future_dataframe(periods=forecast_period, freq=forecast_horizon)
  forecast = model.predict(future)
  forecast = forecast[['ds', 'yhat']]
  return forecast


def multivarite(sku, data, frequency, skus_model, forecast_period, forecast_horizon):
  skus_model[sku] = {}
  col_missing_val, col_to_remove = complication_col(data)
  col_no_missing_val = set(data.columns).difference(set(col_missing_val))
  col_no_missing_val = col_no_missing_val.difference(set(col_to_remove))
  col_no_missing_val = list(col_no_missing_val.difference(set(["y", "ds"])))
  data.drop(col_to_remove, axis=1, inplace=True)
  data.reset_index(drop=True, inplace=True)
  for col in col_missing_val:
    f = missing_val_imputation(sku, col, data, skus_model)
    index = data[col].index[data[col].isnull()]
    c_n = data.columns.get_loc(col)
    for i in index:
      data.iloc[i, c_n] = f.iloc[i, 0]
  for col in col_no_missing_val:
    forecast_missing_val_imputation(sku, col, data, skus_model)
    data.reset_index(drop=True, inplace=True)
    train_df = data
    model = Prophet()
    temp_col = ["y", "ds"]
  for col in data.columns:
    if col not in temp_col:
      model.add_regressor(col, standardize=False)
  model.fit(train_df)
  skus_model[sku]["final_m"] = model
  future = model.make_future_dataframe(periods=forecast_period, freq=forecast_horizon)
  all_features = col_missing_val + col_no_missing_val
  for col in all_features:
    m = skus_model[sku][col]
    future[col] = m.predict(future)['yhat']
  forecast = model.predict(future)
  forecast = forecast[['ds', 'yhat']]
  return forecast


def start_train_forecast(data_csv, target_col, time_col, sku_col, timestamp_format, forecast_period,
                         forecast_horizon, sku_list):
  # setting parameters value in the required format
  df_skus = data_csv.copy()
  logger.info("df_skus: {}".format(df_skus))
  logger.info("time_col,target_col,sku_col,forecast_period: {} {} {} {}".format(
      time_col, target_col, sku_col, forecast_period))
  df_skus[time_col] = pd.to_datetime(df_skus[time_col], format=timestamp_format)
  for c in df_skus.columns:
    if df_skus[c].dtypes == 'object' and c != sku_col:
      df_skus = df_skus.loc[:, ~df_skus.columns.isin([c])]
  logger.info("df after initial clean up {}".format(df_skus))
  data_checking = inputs_checking(df_skus, target_col, time_col, sku_col)
  final_df = data_checking.check_target_col()
  df = final_df.copy()
  logger.info("df : {}".format(df))
  df.rename(columns={target_col: "y", time_col: "ds"}, inplace=True)
  skus = df[sku_col].unique()
  logger.info("skus: {}".format(skus))
  frequency = get_frequency(df, sku_col, skus)
  # start training then forecasting
  skus_model = {}
  result_df = pd.DataFrame()
  if len(df.columns) == 3:
    for sku in sku_list:
      sales_df = pd.DataFrame()
      print("SKU : ", sku)
      if df[sku_col].dtypes == "int":
        sku = int(sku)
      temp_df = df.groupby([sku_col]).get_group(sku)
      sku_df = temp_df.copy()
      sku_df.drop([sku_col], axis=1, inplace=True)
      t_df = univarite(sku, sku_df, frequency, skus_model, forecast_period, forecast_horizon)
      t_df[sku_col] = sku
      result = pd.DataFrame(temp_df[["ds", "y", sku_col]])
      result = result.rename(columns={"y": "yhat"})
      data_fc = pd.concat([sales_df, t_df])
      if frequency and forecast_horizon == "w":
        data_fc['ds'] = pd.to_datetime(data_fc['ds']).apply(pd.DateOffset(1))
      data_fc_new = pd.concat([result, data_fc.tail(forecast_period)], axis=0)
      result_df = pd.concat([result_df, data_fc_new], axis=0)
  else:
    for sku in sku_list:
      sales_df = pd.DataFrame()
      print("SKU : ", sku)
      if df[sku_col].dtypes == "int":
        sku = int(sku)
      temp_df = df.groupby([sku_col]).get_group(sku)
      sku_df = temp_df.copy()
      sku_df.drop([sku_col], axis=1, inplace=True)
      t_df = multivarite(sku, sku_df, frequency, skus_model, forecast_period, forecast_horizon)
      t_df[sku_col] = sku
      result = pd.DataFrame(temp_df[["ds", "y", sku_col]])
      result = result.rename(columns={"y": "yhat"})
      data_fc = pd.concat([sales_df, t_df])
      if frequency and forecast_horizon == "w":
        data_fc['ds'] = pd.to_datetime(data_fc['ds']).apply(pd.DateOffset(1))
      data_fc_new = pd.concat([result, data_fc.tail(forecast_period)], axis=0)
      result_df = pd.concat([result_df, data_fc_new], axis=0)

  other_details = {"variate": len(df.columns), "frequency": frequency, "sku_col": sku_col}
  skus_model["other_details"] = other_details
  logger.info("sales_df: {}".format(data_fc))
  result_df['yhat'] = result_df['yhat'].astype(int)
  # result_df['ds'] = result_df['ds'].dt.date
  return result_df
