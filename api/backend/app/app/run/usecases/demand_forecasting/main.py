import sys

import pandas as pd
import train_forecast
from app.run.usecases.services.aiq_data_set_source import AiqDatasetSource
from app.run.usecases.util.logger import logger
import os
from app.run.usecases.demand_forecasting import mape
from app.run.usecases.services.api_client import ApiClient
import csv

model_version_ref_id = os.getenv('MODEL_VERSION_REF_ID')
source_ref_id = os.getenv('SOURCE_REF_ID')

logger.info("source_ref_id: {}".format(source_ref_id))
logger.info("model_version_ref_id: {}".format(model_version_ref_id))

try:
  api_client = ApiClient(model_version_ref_id, source_ref_id)
  data = {"status": "RUNNING"}
  api_client.update_model_version(data)
  datasource = api_client.get_source()
  model_version = api_client.get_model_version()

  target_col = str(model_version["parameters"]["target_column"])
  input_columns = model_version["parameters"]["input_columns"]

  logger.info("input_columns: {}".format(input_columns))

  for input in input_columns:
    if input.get("time_col", None):
      time_col = str(input["time_col"])
    if input.get("sku_col", None):
      sku_col = str(input["sku_col"])
    if input.get("timestamp_format", None):
      timestamp_format = input["timestamp_format"]  # "%Y-%m-%d"
    if input.get("forecast_period", None):
      forecast_period = input["forecast_period"]
    if input.get("validation_start_dt", None):
      validation_start_dt = input["validation_start_dt"]
    if input.get("forecast_horizon", None):
      forecast_horizon = input["forecast_horizon"]
    if input.get("sku", None):
      sku = input["sku"]
      sku_list = sku.split(",")

  data_path = AiqDatasetSource(datasource).download_data()
  logger.debug("time_col: {}".format(time_col))
  logger.debug("target_col {}".format(target_col))
  logger.debug("sku_col {}".format(sku_col))
  logger.info("data_path: {}".format(data_path))

  logger.info("forecast_horizon: {}".format(forecast_horizon))
  logger.info("sku_list : {}".format(sku_list))

  data_csv = pd.read_csv(data_path)
  logger.info("data_csv:{}".format(data_csv))

  final = pd.DataFrame(
      data_csv[["{}".format(time_col), "{}".format(target_col), "{}".format(sku_col)]])
  final = final.rename(columns={time_col: "ds", target_col: "yhat"})

  # data_csv.drop(["product_ID"], axis=1, inplace=True)

  mape_data = mape.start_validation(data_csv, target_col, time_col, sku_col, timestamp_format)
  data_fc = train_forecast.start_train_forecast(data_csv, target_col, time_col, sku_col,
                                                timestamp_format, forecast_period, forecast_horizon,
                                                sku_list)
except Exception as e:
  logger.error("Exception raised while running the workflow {}".format(str(e)))
  data = {"status": "ERROR", "status_message": "Exception raised while running the workflow"}
  api_client.update_model_version(data)
  sys.exit(1)

logger.info("Obtained sales_data")
path = "/tmp/sales_data.csv"

data_fc.to_csv(path, index=False)

logger.info("sales_data_csv: {}".format(data_fc))
data_file = {"file": open(path, 'r')}
data = {"model_version_ref_id": model_version_ref_id}

logger.info("sending the request for api to return sales_data")

api_client.send_forecast_result_to_api(data_file, data)

# preparing and sending mape score to api

path = "/tmp/mape.csv"
mape_data.to_csv(path, index=False)

with open(path, 'r') as csvfile:
  csv_reader = csv.reader(csvfile)
  fields = next(csv_reader)

  rows = []
  for row in csv_reader:
    rows.append(row)

metrics = []
for i in range(len(rows)):
  dict = {}
  dict[fields[0]] = rows[i][0]
  dict[fields[1]] = rows[i][1]
  metrics.append(dict)
logger.debug("mape object: {}".format(metrics))

data = {}
data["metrics"] = metrics
data["feature_significance"] = None

logger.info("Sending mape score to api")
api_client.update_model_version(data)

# sending status as "success" to update the model version

data = {"status": "SUCCESS"}
api_client.update_model_version(data)
