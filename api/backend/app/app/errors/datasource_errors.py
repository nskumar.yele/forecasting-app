from fastapi import HTTPException


class DataSourceNotFoundException(HTTPException):

  def __init__(self, message):
    super(DataSourceNotFoundException, self).__init__(status_code=404, detail=message)


class DataSourceException(HTTPException):

  def __init__(self, message):
    super(DataSourceException, self).__init__(status_code=500, detail=message)
