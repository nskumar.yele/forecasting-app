from fastapi import HTTPException


class ModelVersionNotFoundException(HTTPException):

  def __init__(self, message):
    super(ModelVersionNotFoundException, self).__init__(status_code=404, detail=message)


class ModelVersionException(HTTPException):

  def __init__(self, message):
    super(ModelVersionException, self).__init__(status_code=500, detail=message)
