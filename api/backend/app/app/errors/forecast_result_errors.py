from fastapi import HTTPException


class ForecastNotFoundException(HTTPException):

  def __init__(self, message):
    super(ForecastNotFoundException, self).__init__(status_code=404, detail=message)


class ForecastException(HTTPException):

  def __init__(self, message):
    super(ForecastException, self).__init__(status_code=500, detail=message)
