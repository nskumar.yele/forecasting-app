from fastapi import HTTPException


class ModelNotFoundException(HTTPException):

  def __init__(self, message):
    super(ModelNotFoundException, self).__init__(status_code=404, detail=message)


class ModelException(HTTPException):

  def __init__(self, message):
    super(ModelException, self).__init__(status_code=500, detail=message)
