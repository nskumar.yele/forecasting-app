# Changelog
All notable changes to this project will be documented in this file. This project adheres to
[Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- US322 working version of fastapi cookiecutter template 
- US324 Added data source CRUD apis and schema for data source tables

### Changed

### Fixed
